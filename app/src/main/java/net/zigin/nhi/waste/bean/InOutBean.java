package net.zigin.nhi.waste.bean;

/**
 * 出入库bean
 */
public class InOutBean {
    private String status;
    private String time;
    private String wasteClassifyCode;
    private String wasteClassifyName;
    private String weight;
    private String fillingType;

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public void setFillingType(String fillingType) {
        this.fillingType = fillingType;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public String getFillingType() {
        return fillingType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWasteClassifyCode() {
        return wasteClassifyCode;
    }

    public void setWasteClassifyCode(String wasteClassifyCode) {
        this.wasteClassifyCode = wasteClassifyCode;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
