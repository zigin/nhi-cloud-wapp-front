package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BoxOutBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.view.depotout.BatchItemOutActivity;
import net.zigin.nhi.waste.view.depotout.BoxOutCheckActivity;
import net.zigin.nhi.waste.view.depotout.OutPrintActivity;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;


public class BoxOutAdapter extends RecyclerView.Adapter<BoxOutAdapter.ViewHolder>{

    public List<BoxOutBean> list;

    public BoxOutAdapter(List<BoxOutBean> list) {
        this.list = list;
    }

    public void notifyData(List<BoxOutBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_box_out, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint({"RecyclerView", "DefaultLocale"})
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BoxOutBean bean = list.get(position);
        holder.mWasteNum.setText(bean.getCode());
        double weight = Double.parseDouble(bean.getCollectWeight());
        holder.mWeight.setText(String.format("%.2f", weight));
        holder.mHospitalName.setText(bean.getHospitalBaseName());

        switch (bean.getRealClassifyCode()) {
            case "chemical":
                holder.mType.setText("化学性");
                break;
            case "drug":
                holder.mType.setText("药物性");
                break;
            case "infection":
                holder.mType.setText("感染性");
                break;
            case "injury":
                holder.mType.setText("损伤性");
                break;
            case "pathology":
                holder.mType.setText("病理性");
                break;
        }

        holder.mDepot.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), BoxOutCheckActivity.class);
            intent.putExtra("qrCode", bean.getContent());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mWeight;
        private TextView mWasteNum;
        private TextView mHospitalName;
        private TextView mType;
        private Button mDepot;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mWeight = itemView.findViewById(R.id.weight);
            mWasteNum = itemView.findViewById(R.id.waste_num);
            mHospitalName = itemView.findViewById(R.id.hospitalName);
            mType = itemView.findViewById(R.id.type);
            mDepot = itemView.findViewById(R.id.depot);
        }
    }
}
