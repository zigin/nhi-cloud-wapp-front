package net.zigin.nhi.waste.net;


import android.content.Context;

import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.view.widget.DialogManager;

import io.paperdb.Paper;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import me.goldze.mvvmhabit.utils.ToastUtils;


public abstract class ApiCall<T> implements Observer<BaseResponse<T>> {

    @Override
    public void onComplete() {
        DialogManager.getInstance().dismissLoading();
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        error(e.toString());
        onComplete();
    }

    @Override
    public void onNext(BaseResponse<T> javaApi) {
        DialogManager.getInstance().dismissLoading();
        if (javaApi.getSuccess()) {
            success(javaApi.getData());
        } else {
            error(javaApi.getMessage());
        }
        onComplete();
    }

    protected abstract void success(T data);

    public void error(String msg) {
        if (msg.contains("401")) {
            ToastUtils.showShortSafe("身份信息失效，请重新登录！");
        } else {
            ToastUtils.showShortSafe(msg);
        }
    }
}
