package net.zigin.nhi.waste.scan;

/**
 * 以广播模式接收PDA扫码结果
 */
public interface CodeListener {

    /**
     * 获取扫描结果
     * @param data  扫码结果
     */
    void getData(String data);
}
