package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.AlarmBean;
import net.zigin.nhi.waste.bean.InOutBean;
import net.zigin.nhi.waste.bean.RecordBean;

import java.util.ArrayList;
import java.util.List;

public class DepotHistoryAdapter extends RecyclerView.Adapter<DepotHistoryAdapter.ViewHolder> {

    public List<InOutBean> list;

    public DepotHistoryAdapter(List<InOutBean> list) {
        this.list = list;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyData(List<InOutBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_depot_history, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        InOutBean bean = list.get(position);
        holder.mTime.setText(bean.getTime());
        holder.mWeight.setText(bean.getWeight());
        holder.mType.setText(bean.getWasteClassifyName());
        if("1".equals(bean.getFillingType())){
            holder.tv_name.setText("数量：");
            holder.tv_unit.setText("个");
        }else{
            holder.tv_name.setText("重量：");
            holder.tv_unit.setText("kg");
        }
        switch (bean.getStatus()) {
            case "out_depot":
                holder.mStatus.setText("已出库");
                holder.mImgStatus.setImageResource(R.drawable.depot_out);
                break;
            case "do_collect":
                holder.mStatus.setText("未入库");
                holder.mImgStatus.setImageResource(R.drawable.depot_no);
                break;
            case "in_depot":
                holder.mStatus.setText("已入库");
                holder.mImgStatus.setImageResource(R.drawable.depot_in);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImgStatus;
        private TextView mTime;
        private TextView mWeight;
        private TextView mType;
        private TextView mStatus,tv_name,tv_unit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImgStatus = itemView.findViewById(R.id.img_status);
            mTime = itemView.findViewById(R.id.time);
            mWeight = itemView.findViewById(R.id.weight);
            mType = itemView.findViewById(R.id.type);
            mStatus = itemView.findViewById(R.id.status);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_unit = itemView.findViewById(R.id.tv_unit);
        }
    }
}
