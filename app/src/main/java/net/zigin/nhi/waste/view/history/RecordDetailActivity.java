package net.zigin.nhi.waste.view.history;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.adapter.RecordAdapter;
import net.zigin.nhi.waste.bean.RecordBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 医废详细记录
 */
public class RecordDetailActivity extends AppCompatActivity {

    private RecyclerView mRvRecord;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private RecordAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_detail);

        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mRvRecord = findViewById(R.id.rv_record);
        mRvRecord.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDetail();
    }

    /**
     * 获取医废详细记录
     */
    private void getDetail() {
        Intent intent = getIntent();
        String wasteBaseId = intent.getStringExtra("recordID");

        List<RecordBean> recordBeans = Paper.book().read("record_list");
        if (recordBeans != null && recordBeans.size() > 0) {
            adapter = new RecordAdapter(recordBeans);
            mRvRecord.setAdapter(adapter);
        }

        if (StringUtil.isNotEmpty(wasteBaseId)) {
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .getRecordList(Paper.book().read(Constants.TOKEN), wasteBaseId);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    if (StringUtil.isNotEmpty(data)) {
                        Log.i("追溯详细", data);
                        List<RecordBean> list = JSONArray.parseArray(data, RecordBean.class);
                        if (list != null && list.size() > 0) {
                            adapter = new RecordAdapter(list);
                            mRvRecord.setAdapter(adapter);
                        } else {
                            mRvRecord.setAdapter(noDataAdapter);
                        }
                    } else {
                        mRvRecord.setAdapter(noDataAdapter);
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Paper.book().delete("record_list");
    }
}