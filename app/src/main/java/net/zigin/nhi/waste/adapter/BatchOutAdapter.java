package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BatchInBean;
import net.zigin.nhi.waste.bean.BatchOutBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.view.depotout.BatchItemOutActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class BatchOutAdapter extends RecyclerView.Adapter<BatchOutAdapter.ViewHolder>{

    public List<BatchOutBean> list;
    private IActivityUpData dd;
    private TextView textView;
    private boolean isItemCheck = false;

    private Set<BatchOutBean> newList;

    public BatchOutAdapter(List<BatchOutBean> list, IActivityUpData dd,TextView textView) {
        this.list = list;
        this.dd = dd;
        this.textView = textView;
        newList = new HashSet<>();
    }

    public void notifyData(List<BatchOutBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_out_batch, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BatchOutBean bean = list.get(position);
        holder.mWeight.setText(bean.getWeight());
        holder.mWasteNum.setText(bean.getCode());
        holder.mHospitalName.setText(bean.getHospitalBaseName());
        holder.mDepartName.setText(bean.getHospitalDepartName());
        holder.mConsignee.setText(bean.getCollectUserStaffName());
        holder.mPublisher.setText(bean.getHandUserStaffName());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mTime.setText(bean.getCreateTime());
        if("1".equals(bean.getFillingType())){
            holder.tv_name.setText("收集数量：");
            holder.tv_unit.setText("个");
        }else{
            holder.tv_name.setText("收集重量：");
            holder.tv_unit.setText("kg");
        }

        boolean isCheck = bean.isSelect();
        if (isCheck) {
            isItemCheck = true;
            holder.mImg.setImageResource(R.drawable.depot_in_selected);
        } else {
            isItemCheck = false;
            holder.mImg.setImageResource(R.drawable.depot_in_unselected);
        }

        newList.add(bean);
        Paper.book().write(Constants.NEW_BATCH_OUT_LIST, newList);

        holder.mLayout.setOnClickListener(v -> {
            if (isItemCheck) {
                newList.remove(bean);
                isItemCheck = false;
                holder.mImg.setImageResource(R.drawable.depot_in_unselected);
                bean.setSelect(false);
                dd.upDataUi();

                newList.add(bean);
                Paper.book().write(Constants.NEW_BATCH_OUT_LIST, newList);
            } else {
                newList.remove(bean);
                isItemCheck = true;
                holder.mImg.setImageResource(R.drawable.depot_in_selected);
                bean.setSelect(true);

                newList.add(bean);
                Paper.book().write(Constants.NEW_BATCH_OUT_LIST, newList);
            }
            float totalWeight=0.0F;
            for (BatchOutBean batchOutBean : newList) {
                if(!"1".equals(batchOutBean.getFillingType())&&batchOutBean.isSelect()){
                    float weight= Float.parseFloat(batchOutBean.getWeight());
                    totalWeight=totalWeight+weight;
                }
            }
            textView.setText(String.valueOf(totalWeight));
        });

        holder.mDepot.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), BatchItemOutActivity.class);
            intent.putExtra("wasteItemID", bean.getId());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLayout;
        private ImageView mImg;
        private TextView mWasteNum;
        private TextView mWeight;
        private TextView mHospitalName;
        private TextView mDepartName;
        private TextView mType;
        private TextView mPublisher;
        private TextView mConsignee;
        private TextView mTime;
        private TextView tv_name;
        private TextView tv_unit;
        private Button mDepot;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout);
            mImg = itemView.findViewById(R.id.img);
            mWasteNum = itemView.findViewById(R.id.waste_num);
            mWeight = itemView.findViewById(R.id.weight);
            mHospitalName = itemView.findViewById(R.id.hospitalName);
            mDepartName = itemView.findViewById(R.id.departName);
            mType = itemView.findViewById(R.id.type);
            mPublisher = itemView.findViewById(R.id.publisher);
            mConsignee = itemView.findViewById(R.id.consignee);
            mTime = itemView.findViewById(R.id.time);
            tv_unit = itemView.findViewById(R.id.tv_unit);
            tv_name = itemView.findViewById(R.id.tv_name);
            mDepot = itemView.findViewById(R.id.depot);
        }
    }
}
