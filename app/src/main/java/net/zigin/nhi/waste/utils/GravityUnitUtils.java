package net.zigin.nhi.waste.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import net.zigin.nhi.waste.constants.Constants;

public class GravityUnitUtils {
    /**
     * 将接收到的结果按单位匹配，只取重量的数字
     * @param data 打开通知后接收到的数据
     * @return
     */
    @SuppressLint("DefaultLocale")
    public String getGravity(Context context, String data) {
        try {
            String unit = "";
            String gravity = "0.0";
            if (Constants.BLEDEVICE.getName().contains("HC-")) {
                if (data.contains("weight")) {  //蓝牙大秤
                    if (data.contains("Unit")) {
                        String result = data.substring(7).trim();
                        String temp = result.replaceAll(" ","").replaceAll("\\r","").replaceAll("\\b","")
                                .replaceAll("\\n","").replaceAll("weightUnit:","");
                        Log.i("最终重量", temp);
                        gravity = temp.replaceAll("[a-zA-Z]", "");
                        unit = temp.replace(gravity, "");
                        Log.i("单位--------", unit);
                        switch (unit) {
                            case "kg":
                                break;
                            case "g":
                                double res = Double.parseDouble(gravity) / 1000.0;
                                gravity = String.format("%.2f", res);
                                break;
                            case "lb":
                                double res1 = Double.parseDouble(gravity) / 2.2046;
                                gravity = String.format("%.2f", res1);
                                break;
                            case "Cj":
                                double res2 = Double.parseDouble(gravity) / 2;
                                gravity = String.format("%.2f", res2);
                                break;
                            case "Tj":
                            case "Hj":
                                double res3 = Double.parseDouble(gravity) / 1.67;
                                gravity = String.format("%.2f", res3);
                                break;
                            case "N":
                                double res4 = Double.parseDouble(gravity) / 10;
                                gravity = String.format("%.2f", res4);
                                break;
                            case "Tjl":
                                Toast.makeText(context, "暂不支持此单位换算，请切换单位！", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    } else {
                        Toast.makeText(context, "请在蓝牙秤参数设置中勾选重量单位！", Toast.LENGTH_SHORT).show();
                    }
                } else {  //地磅
                    String result = data.substring(6, 17).trim();
                    String s = result.substring(result.getBytes().length - 2, result.getBytes().length);
                    //单位
                    unit = s.replace(" ", "");
                    Log.i("单位--------", unit);

                    String temp = data.substring(6, data.getBytes().length - 4);
                    gravity = temp.replaceAll(" ","").replaceAll("\\r","").replaceAll("\\b","")
                            .replaceAll("\\n","");

                    //单位换算成kg 保留小数点后两位
                    switch (unit) {
                        case "kg":
                            gravity = gravity + "0";
                            break;
                        case "g":
                            double res = Double.parseDouble(gravity) / 1000.0;
                            gravity = String.format("%.2f", res);
                            break;
                        case "lb":
                            double res1 = Double.parseDouble(gravity) / 2.2046;
                            gravity = String.format("%.2f", res1);
                            break;
                        case "oz":
                            double res2 = Double.parseDouble(gravity) / 35.2739619;
                            gravity = String.format("%.2f", res2);
                            break;
                    }
                }
            } else if (Constants.BLEDEVICE.getName().contains("Furi")) {  //手提秤
                String result = data.substring(7, 17).trim();
                String s = result.substring(result.getBytes().length - 2, result.getBytes().length);
                //单位
                unit = s.replace(" ", "");
                Log.i("单位--------", unit);

                String temp = data.substring(7, data.getBytes().length - 4);
                gravity = temp.replaceAll(" ","").replaceAll("\\r","").replaceAll("\\b","")
                        .replaceAll("\\n","");
            }
            Log.i("重量--------", gravity);
            return gravity;
        } catch (Exception e) {
            e.printStackTrace();
            return "0.00";
        }
    }
}
