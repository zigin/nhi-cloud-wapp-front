package net.zigin.nhi.waste.utils;

import android.util.Log;

/**
 * @Author:
 * @Description:
 */
public class CommonUtils {

    // 两次点击时间不低于500
    private static final int MIN_CLICK_DELAY_TIME = 500;
    private static long lastClickTime;

    public static boolean isFastClick() {//防止重复点击
        boolean flag = true;
//        long oldLastClickTime = lastClickTime;
        long currentClickTime = System.currentTimeMillis();
        long delay = currentClickTime - lastClickTime;
        Log.d("isFastClick", "currentClickTime: " + currentClickTime + ", lastClickTime: " + lastClickTime);
        if (delay >= MIN_CLICK_DELAY_TIME) {
            flag = false;
            lastClickTime = currentClickTime;
        }

        return flag;
    }
}
