package net.zigin.nhi.waste.view.collect;

import android.content.Intent;
import android.os.Bundle;
import android.os.IScanListener;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gyf.immersionbar.ImmersionBar;
import com.yzq.zxinglibrary.android.CaptureActivity;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.CommonUtils;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

import static net.zigin.nhi.waste.MainApplication.miScanInterface;


/**
 * 医废收集移交人扫码界面
 */
public class HandoverScanActivity extends AppCompatActivity{
    private LinearLayout mScan;
    private Map<String, Object> map;
    private String fillingType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_handover_scan);
        fillingType=getIntent().getStringExtra("fillingType");
        initView();
        miScanInterface.registerScan(miScanListener);
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mScan = findViewById(R.id.scan);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mScan.setOnClickListener(v -> {
            if (miScanInterface != null) {
                //开启扫描
                miScanInterface.scan_start();
            } else {
                Intent intent = new Intent(HandoverScanActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 1003);
            }
        });
    }

    /**
     * 医废收集
     * @param data
     */
    private void collect(String data) {
        if(CommonUtils.isFastClick()){
            return;
        }
        if (StringUtil.isNotEmpty(data)) {
            map = Paper.book().read(Constants.COLLECTMAP);
            map.put("handUserQrCode", data);
            map.put("fillingType", fillingType);
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .wasteCollect(Paper.book().read(Constants.TOKEN).toString(), map);
            DialogManager.getInstance().showLoading(this);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Log.i("医废收集", data);
                    Paper.book().write(Constants.COLLECTID, data);
                    Intent intent = new Intent(HandoverScanActivity.this, CollectPrintActivity.class);
                    startActivity(intent);
                    //finish();//别清掉可能扫错返回重扫
                }

            });
        }
    }

    //数据回调监听
    private IScanListener miScanListener = new IScanListener() {

        /*
         * param data 扫描数据
         * param type 条码类型
         * param decodeTime 扫描时间
         * param keyDownTime 按键按下的时间
         * param imagePath 图片存储地址，通常用于ocr识别需求（需要先开启保存图片才有效）
         */
        @Override
        public void onScanResults(String data, int type, long decodeTime, long keyDownTime,String imagePath) {

            //解码失败
            if(data == null || data.isEmpty()){
                data = "  decode error ";
                //miScanInterface.scan_start();
            }else{
                miScanInterface.scan_stop();
                String finalData = data;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        collect(finalData);
                    }
                });
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == 1003 && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra("codedContent");
                Log.i("扫码结果：", content);
                collect(content);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        miScanInterface.unregisterScan(miScanListener);
    }
}