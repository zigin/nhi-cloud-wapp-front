package net.zigin.nhi.waste.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.FilterTypeAdapter;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.view.depotin.BatchBindActivity;
import net.zigin.nhi.waste.view.depotin.BoxBindActivity;
import net.zigin.nhi.waste.view.depotin.ScanBoxActivity;
import net.zigin.nhi.waste.view.depotin.ScanWasteActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

/**
 * 添加绑定对话框
 */
public class AddBindDialog extends Dialog implements View.OnClickListener {

    private View mViewNone;
    private Button mClose;
    private LinearLayout mScanBind;
    private LinearLayout mPackageBind;

    private Activity activity;

    public AddBindDialog(@NonNull Context context, Activity activity) {
        super(context);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_bind);
        initView();
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mViewNone = findViewById(R.id.view_none);
        mClose = findViewById(R.id.close);
        mScanBind = findViewById(R.id.scan_bind);
        mPackageBind = findViewById(R.id.package_bind);

        Window window = this.getWindow();
        window.setBackgroundDrawable(null);
        // 设置宽高
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置弹出的动画效果
        window.setWindowAnimations(R.style.AnimBottom);
        mViewNone = window.findViewById(R.id.view_none);
        mViewNone.getBackground().setAlpha(0);

        mViewNone.setOnClickListener(this);
        mClose.setOnClickListener(this);
        mScanBind.setOnClickListener(this);
        mPackageBind.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_none:    //空白处
            case R.id.close:        //关闭按钮
                dismiss();
                ImmersionBar.destroy(activity, this);
                break;
            case R.id.scan_bind:    //扫码绑定
                Intent intent = new Intent();
                intent.setClass(activity, ScanWasteActivity.class);
                intent.putExtra("bind", "bind");
                activity.startActivity(intent);
                activity.finish();
                break;
            case R.id.package_bind: //袋装绑定
                Intent intent2 = new Intent(activity, BatchBindActivity.class);
                activity.startActivity(intent2);
                break;
        }
    }
}
