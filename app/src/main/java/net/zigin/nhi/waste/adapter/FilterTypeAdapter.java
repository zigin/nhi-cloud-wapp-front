package net.zigin.nhi.waste.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.WastePrintBean;
import net.zigin.nhi.waste.constants.Constants;

import java.util.List;
import java.util.Map;

import io.paperdb.Paper;

public class FilterTypeAdapter extends RecyclerView.Adapter<FilterTypeAdapter.ViewHolder>{

    public List<WastePrintBean> list;
    private int mPosition=0;
    private Map<String, Object> map;

    public FilterTypeAdapter(List<WastePrintBean> list, Map<String, Object> map) {
        this.list = list;
        this.map = map;
    }

    public interface GetListener {
        void onClick(int position, String text);
    }

    private GetListener getListener;

    public void setGetListener(GetListener getListener) {
        this.getListener = getListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_type, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String text = list.get(position).getWasteClassifyName().trim();
        holder.mTv.setText(text);

        holder.mLayout.setOnClickListener(v -> {
            setPosition(position);
            if(text.equals("全部")){
                map.remove("wasteClassifyCode");
                Paper.book().delete(Constants.WASTE_CLASSIFY_CODE);
            }else{
                map.put("wasteClassifyCode", list.get(position).getWasteClassifyCode());
                Paper.book().write(Constants.WASTE_CLASSIFY_CODE,list.get(position).getWasteClassifyCode());
            }
            notifyDataSetChanged();
        });

        if (position == getPosition()) {
            holder.mLayout.setBackgroundResource(R.drawable.bg_type_filter_selected);
            holder.mTv.setTextColor(Color.parseColor("#34BAFF"));
        }else{
            holder.mLayout.setBackgroundResource(R.drawable.bg_type_filter);
            holder.mTv.setTextColor(Color.parseColor("#333333"));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout mLayout;
        private TextView mTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout);
            mTv = itemView.findViewById(R.id.tv);
        }
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        this.mPosition = position;
    }
}
