package net.zigin.nhi.waste.adapter;

import net.zigin.nhi.waste.bean.AlarmBean;
import net.zigin.nhi.waste.bean.HistoryBean;

import java.util.List;

/**
 * 医废追溯筛选对话框通知activity的adapter更新list数据源
 */
public interface IActivityHistoryType {
    void upDataUi(List<HistoryBean> list);
}
