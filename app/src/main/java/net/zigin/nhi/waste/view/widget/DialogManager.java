package net.zigin.nhi.waste.view.widget;

import android.content.Context;

/**
 * @Author:
 * @Description:
 */
public class DialogManager {
    private volatile static DialogManager sInstance;

    public static DialogManager getInstance() {
        if (sInstance == null) {
            synchronized (DialogManager.class) {
                if (null == sInstance) {
                    sInstance = new DialogManager();
                }
            }
        }

        return sInstance;
    }

    private DialogManager() {
    }

    private YcProgressDialog dialog;

    public void showLoading(Context context) {
        if (dialog == null) {
            dialog = new YcProgressDialog(context);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
        }

        dialog.show();
    }

    public void dismissLoading() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
