package net.zigin.nhi.waste.utils;

public class BleWeightUtil {
    public static String byteToString(byte[] data){
        String weight= "0.0";
        byte [] bytes=new byte[4];
        bytes[0]=data[13];
        bytes[1]=data[14];
        bytes[2]=data[15];
        bytes[3]=data[16];
        int firstByte = 0;
        int secondByte = 0;
        int thirdByte = 0;
        int fourthByte = 0;
        firstByte = (0x000000FF & ((int) bytes[0]));
        secondByte = (0x000000FF & ((int) bytes[1]));
        thirdByte = (0x000000FF & ((int) bytes[2]));
        fourthByte = (0x000000FF & ((int) bytes[3]));

        String ss= String.valueOf(((long) (firstByte << 24 | secondByte << 16 | thirdByte << 8 | fourthByte)) & 0xFFFFFFFFL);
        try{
            weight= String.format("%.2f",Integer.parseInt(ss)*0.001);
        }catch (NumberFormatException e){
            return "-1";
        }
        return weight;
    }
}
