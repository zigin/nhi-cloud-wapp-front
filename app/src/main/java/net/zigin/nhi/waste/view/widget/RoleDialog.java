package net.zigin.nhi.waste.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.FilterDepartAdapter;
import net.zigin.nhi.waste.adapter.IDialogUpData;
import net.zigin.nhi.waste.bean.DepartBean;
import net.zigin.nhi.waste.bean.RoleBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class RoleDialog extends Dialog {

    private View mViewNone;
    private Button mClose;
    private RecyclerView mRvRole;
    private Button mConfirm;
    private Activity activity;
    private IDialogUpData dd;
    public static RoleDialog mInstance;

    private List<String> roleList = new ArrayList<>();
    private String roleName;
    private String roleId;

    public RoleDialog(@NonNull Context context, Activity activity, IDialogUpData dd) {
        super(context);
        this.activity = activity;
        this.dd = dd;
    }

    //单例保证每次弹出的dialog唯一
    public static RoleDialog getInstance(Context context, Activity activity, IDialogUpData dd) {
        if (mInstance == null) {
            synchronized (RoleDialog.class) {
                if (mInstance == null) {
                    mInstance = new RoleDialog(context, activity, dd);
                }
            }
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_role);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mViewNone = findViewById(R.id.view_none);
        mClose = findViewById(R.id.close);
        mRvRole = findViewById(R.id.rv_role);
        mConfirm = findViewById(R.id.confirm);

        Window window = this.getWindow();
        window.setBackgroundDrawable(null);
        // 设置宽高
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置弹出的动画效果
        window.setWindowAnimations(R.style.AnimBottom);
        mViewNone = window.findViewById(R.id.view_none);
        mViewNone.getBackground().setAlpha(0);
    }


    private void initData() {
        mViewNone.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mClose.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        getRole();

        mConfirm.setOnClickListener(v -> {
            dd.upDataUi(roleName, roleId);
            dismiss();
        });
    }

    /**
     * 获取科室列表
     */
    private void getRole() {
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getRoleList(Paper.book().read(Constants.TOKEN));
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                roleList.clear();
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("角色列表", data);
                    List<RoleBean> list = JSONArray.parseArray(data, RoleBean.class);
                    for (RoleBean bean : list) {
                        roleList.add(bean.getName());
                    }
                    FilterDepartAdapter departAdapter = new FilterDepartAdapter(roleList);
                    mRvRole.setLayoutManager(new GridLayoutManager(activity, 3));
                    mRvRole.setAdapter(departAdapter);
                    departAdapter.setGetListener((position, text) -> {
                        departAdapter.setPosition(position);
                        departAdapter.notifyDataSetChanged();

                        roleName = list.get(position).getName();
                        roleId = list.get(position).getId();
                    });
                }
            }
        });
    }
}
