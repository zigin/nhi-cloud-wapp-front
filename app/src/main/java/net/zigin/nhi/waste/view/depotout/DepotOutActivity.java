package net.zigin.nhi.waste.view.depotout;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.view.depotin.ScanWasteActivity;
import net.zigin.nhi.waste.view.print.OutOrderActivity;
import net.zigin.nhi.waste.view.rfid.ScanRfidActivity;

import io.paperdb.Paper;

/**
 * 医废出库界面
 */
public class DepotOutActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mScanLayout;
    private LinearLayout mBindLayout;
    private LinearLayout mRfidLayout;
    private LinearLayout mBoxLayout;
    private TextView mGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_depot_out);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mScanLayout = findViewById(R.id.scan_layout);
        mBindLayout = findViewById(R.id.bind_layout);
        mRfidLayout = findViewById(R.id.rfid_layout);
        mBoxLayout = findViewById(R.id.box_layout);
        mGo = findViewById(R.id.go);

        mScanLayout.setOnClickListener(this);
        mBindLayout.setOnClickListener(this);
        mRfidLayout.setOnClickListener(this);
        mBoxLayout.setOnClickListener(this);
        mGo.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Paper.book().delete(Constants.WASTE_SCAN_DTOS);
        Paper.book().delete(Constants.WASTE_BATCH_ITEM_DTOS);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scan_layout:  //扫码出库
                Intent intent1 = new Intent();
                intent1.setClass(DepotOutActivity.this, ScanWasteActivity.class);
                intent1.putExtra("scan_out", "scan_out");
                startActivity(intent1);
                break;
            case R.id.bind_layout:  //批量出库
                Intent intent2 = new Intent(DepotOutActivity.this, BatchOutActivity.class);
                startActivity(intent2);
                break;
            /*case R.id.rfid_layout:  //RFID出库
//                Intent intent3 = new Intent(DepotOutActivity.this, ScanPDARfidActivity.class);
                Intent intent3 = new Intent(DepotOutActivity.this, ScanRfidActivity.class);
                startActivity(intent3);
                break;*/
            case R.id.box_layout:   //集装箱出库
                Intent intent4 = new Intent(DepotOutActivity.this, BoxOutActivity.class);
                startActivity(intent4);
                break;
            case R.id.go:           //去补打
                Intent intent5 = new Intent(DepotOutActivity.this, OutOrderActivity.class);
                startActivity(intent5);
                break;
        }
    }
}