package net.zigin.nhi.waste.view.print;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.siberiadante.titlelayoutlib.TitleBarLayout;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.BoxLabelAdapter;
import net.zigin.nhi.waste.adapter.IActivityUpData;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.bean.BoxLabelBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.CommonUtils;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.depotout.OutPrintActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.paperdb.Paper;
import io.reactivex.Observable;

import static net.zigin.nhi.waste.MainApplication.oPrinterSdk;

/**
 * 集装箱标签界面
 */
public class BoxLabelActivity extends AppCompatActivity {

    private TitleBarLayout mTitle;
    private SmartRefreshLayout mRefresh;
    private RecyclerView mRv;
    private ImageView mImg;
    private Button mPrint;
    private WebView mWebView;

    private BoxLabelAdapter adapter;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private boolean isSelected = false;
    private List<BoxLabelBean> beanList = new ArrayList<>();

    private int page = 1;
    private int limit = 20;
    private int index=0;//当前发送的第几张图
    private int bitmapSize=0;//当前任务图片数
    List<BoxLabelBean> webList = new ArrayList<>();   //给webView传参的list
    private int connectStatus=-1;//打印机连接状态

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_label);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mTitle = findViewById(R.id.title);
        mRefresh = findViewById(R.id.refresh);
        mRv = findViewById(R.id.rv);
        mImg = findViewById(R.id.img);
        mWebView = findViewById(R.id.web_view);
        mPrint = findViewById(R.id.print);
        mRv.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initData() {
        mRefresh.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                beanList.clear();
                page = 1;
                getBoxLabel();
            }
        });

        mRefresh.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                HashSet<String> ids = new HashSet<>();
                Map<String, Object> map = new HashMap<>();
                map.put("wasteBoxIds", ids);
                map.put("orderBy", "time");
                map.put("isPage", true);
                map.put("pageIndex", page);
                map.put("pageSize", limit);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .getBoxLabelList(Paper.book().read(Constants.TOKEN), map);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String result) {
                        if (StringUtil.isNotEmpty(result)) {
                            Log.i("集装箱标签信息", result);
                            List<BoxLabelBean> newList = JSONArray.parseArray(result, BoxLabelBean.class);
                            if (newList != null && newList.size() > 0) {
                                beanList.addAll(newList);
                                adapter.notifyData(beanList);
                            }
                            if (beanList.size() < limit) {
                                mRefresh.finishLoadMoreWithNoMoreData();
                            } else {
                                mRefresh.finishLoadMore();
                                page++;
                            }
                        } else {
                            mRefresh.finishLoadMoreWithNoMoreData();
                        }
                    }
                });
            }
        });

        mTitle.setRightTextClickListener(v -> {
            Intent intent = new Intent(BoxLabelActivity.this, AddBoxLabelActivity.class);
            startActivity(intent);
        });

        mImg.setOnClickListener(v -> {
            if (isSelected) {
                isSelected = false;
                mImg.setImageResource(R.drawable.depot_in_unselected);

                for (BoxLabelBean bean : beanList) {
                    bean.setSelect(false);
                }
            } else {
                isSelected = true;
                mImg.setImageResource(R.drawable.depot_in_selected);

                for (BoxLabelBean bean : beanList) {
                    bean.setSelect(true);
                }
            }
            if (adapter != null) {
                adapter.notifyData(beanList);
            }
        });

        mPrint.setOnClickListener(v -> {
            if(connectStatus==0){
                mWebView.clearCache(true);webList.clear();
                Set<BoxLabelBean> newList = Paper.book().read(Constants.NEW_BOX_LABEL_LIST);
                if (newList != null && newList.size() > 0) {
                    for (BoxLabelBean bean : newList) {
                        if (bean.isSelect()) {
                            //已选中的打印
                            webList.add(bean);
                        }
                    }
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        index=0;bitmapSize=webList.size();
                        if(bitmapSize>0){
                            String call = "javascript:getLabel('" + JSONObject.toJSONString(webList.get(index)) + "')";
                            runOnUiThread(() -> {
                                mWebView.loadUrl(call);
                            });
                        }
                    }
                }).start();
            }else{
                Toast.makeText(this,"打印机连接中，请稍后重试！",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.enableSlowWholeDocumentDraw();
        }
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存
        mWebView.addJavascriptInterface(new PrintObject(), "connect");
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.loadUrl("file:///android_asset/boxlabel.html");

        initData();
        mRefresh.autoRefresh();
        connect();
    }
    //连接得力打印机
    private void connect(){
        new Thread(){
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                ArrayList<BluetoothDevice> aDevices = oPrinterSdk.GetDevices();
                if(aDevices!=null&&aDevices.size()>0){
                    if(oPrinterSdk.Connect(aDevices.get(0).getAddress()) == 0){
                        connectStatus=0;
                        Toast.makeText(BoxLabelActivity.this,"打印机连接成功",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(BoxLabelActivity.this,"打印机连接失败",Toast.LENGTH_SHORT).show();
                        connect();
                    }
                }else{
                    Toast.makeText(BoxLabelActivity.this,"附近没有搜到打印机！",Toast.LENGTH_SHORT).show();
                }
                Looper.loop();
            }
        }.start();
    }

    /**
     * 获取集装箱标签列表
     */
    private void getBoxLabel() {
        HashSet<String> ids = new HashSet<>();
        Map<String, Object> map = new HashMap<>();
        map.put("wasteBoxIds", ids);
        map.put("orderBy", "time");
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getBoxLabelList(Paper.book().read(Constants.TOKEN), map);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String result) {
                if (StringUtil.isNotEmpty(result)) {
                    Log.i("集装箱标签信息", result);
                    beanList = JSONArray.parseArray(result, BoxLabelBean.class);
                    if (beanList != null && beanList.size() > 0) {
                        adapter = new BoxLabelAdapter(beanList, dd);
                        mRv.setAdapter(adapter);
                        page++;
                    } else {
                        mRv.setAdapter(noDataAdapter);
                    }
                } else {
                    mRv.setAdapter(noDataAdapter);
                }
                mRefresh.finishRefresh();
            }

            @Override
            public void error(String msg) {
                super.error(msg);
                mRefresh.finishRefresh();
                mRv.setAdapter(noDataAdapter);
            }
        });

    }

    /**
     * 子项取消选中时，全选按钮变更状态为未选中
     */
    private IActivityUpData dd = new IActivityUpData() {
        @Override
        public void upDataUi() {
            isSelected = false;
            mImg.setImageResource(R.drawable.depot_in_unselected);
        }
    };

    class PrintObject {
        @JavascriptInterface
        public void print(final int x, final int y, final int width, final int height) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {

                    } catch (Exception | Error e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    @SuppressLint("HandlerLeak")
    Handler handler=new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            try {
                Thread.sleep(200);//
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int nw = mWebView.getWidth();
            int nh = mWebView.getHeight();
            Bitmap bitmap = Bitmap.createBitmap(nw, nh, Bitmap.Config.RGB_565);
            Canvas can = new Canvas(bitmap);
            mWebView.draw(can);
            int newWidth = nw;
            int newHeight = nh;
            if (nw > 400) {
                float rate = 380 * 1.0f / nw * 1.0f;
                newWidth = 380;
                newHeight = (int) (nh * rate);
            }
            //Thread.sleep(1000);//等待1s绘画完成
            bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
            Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0, newWidth, newHeight);
            if (oPrinterSdk.Tspl_SetSize(60, 40) != 0)return;//设置纸张大小
            if (oPrinterSdk.Tspl_SetGap(2, 0) != 0) return;//设置标签缝宽  连续纸可直接设置为 0 0
            if (oPrinterSdk.Tspl_SetDensity(7) != 0) return;//设置打印浓度0~15
            if (oPrinterSdk.Tspl_SetSpeed(5) != 0) return;//设置打印速度0~8
            if (oPrinterSdk.Tspl_Cls() != 0) return;//清空打印机打印缓存
            if (oPrinterSdk.Vtr_SetBitmap(newBitmap, 0, 0, true) != 0) return;//发送图片
            if (oPrinterSdk.Tspl_Print(1, 1) == 0) {//打印打印机缓存内容
                if(bitmapSize>index+1){
                    index=index+1;
                    String call = "javascript:getLabel('" + JSONObject.toJSONString(webList.get(index)) + "')";
                    runOnUiThread(() -> {
                        mWebView.loadUrl(call);
                    });
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        oPrinterSdk.Disconnect();
    }
}