package net.zigin.nhi.waste.bean;

public class OutOrderBean {
    private String code;
    private String createTime;
    private String createUser;
    private String hospitalBaseId;
    private String id;
    private String type;
    private String userRevicerId;
    private String userRevicerName;
    private String wasteBoxCode;
    private String wasteBoxRecordId;
    private String weight;

    public String getFillingType() {
        return fillingType;
    }

    public void setFillingType(String fillingType) {
        this.fillingType = fillingType;
    }

    private String fillingType;
    private ReceiverBean sysUserRevicerVo;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getHospitalBaseId() {
        return hospitalBaseId;
    }

    public void setHospitalBaseId(String hospitalBaseId) {
        this.hospitalBaseId = hospitalBaseId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserRevicerId() {
        return userRevicerId;
    }

    public void setUserRevicerId(String userRevicerId) {
        this.userRevicerId = userRevicerId;
    }

    public String getUserRevicerName() {
        return userRevicerName;
    }

    public void setUserRevicerName(String userRevicerName) {
        this.userRevicerName = userRevicerName;
    }

    public String getWasteBoxCode() {
        return wasteBoxCode;
    }

    public void setWasteBoxCode(String wasteBoxCode) {
        this.wasteBoxCode = wasteBoxCode;
    }

    public String getWasteBoxRecordId() {
        return wasteBoxRecordId;
    }

    public void setWasteBoxRecordId(String wasteBoxRecordId) {
        this.wasteBoxRecordId = wasteBoxRecordId;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public ReceiverBean getSysUserRevicerVo() {
        return sysUserRevicerVo;
    }

    public void setSysUserRevicerVo(ReceiverBean sysUserRevicerVo) {
        this.sysUserRevicerVo = sysUserRevicerVo;
    }
}
