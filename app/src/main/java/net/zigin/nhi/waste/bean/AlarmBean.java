package net.zigin.nhi.waste.bean;

public class AlarmBean {
    private String createTime;
    private String createUser;
    private String objectCode;
    private String objectId;
    private String objectType;
    private String collectUserStaffName;
    private String handUserStaffName;
    private String hospitalDepartId;
    private String hospitalDepartName;
    private String hospitalPlaceName;
    private String hospitalBaseId;
    private String hospitalBaseName;
    private String id;
    private String status;
    private String time;
    private String type;
    private String wasteBaseId;
    private String wasteClassifyId;
    private String wasteClassifyName;
    private String modifyTime;
    private String modifyUser;
    private String remark;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getHospitalDepartId() {
        return hospitalDepartId;
    }

    public void setHospitalDepartId(String hospitalDepartId) {
        this.hospitalDepartId = hospitalDepartId;
    }

    public String getHospitalBaseId() {
        return hospitalBaseId;
    }

    public void setHospitalBaseId(String hospitalBaseId) {
        this.hospitalBaseId = hospitalBaseId;
    }

    public String getHospitalBaseName() {
        return hospitalBaseName;
    }

    public void setHospitalBaseName(String hospitalBaseName) {
        this.hospitalBaseName = hospitalBaseName;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectCode() {
        return objectCode;
    }

    public void setObjectCode(String objectCode) {
        this.objectCode = objectCode;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCollectUserStaffName() {
        return collectUserStaffName;
    }

    public void setCollectUserStaffName(String collectUserStaffName) {
        this.collectUserStaffName = collectUserStaffName;
    }

    public String getHandUserStaffName() {
        return handUserStaffName;
    }

    public void setHandUserStaffName(String handUserStaffName) {
        this.handUserStaffName = handUserStaffName;
    }

    public String getHospitalDepartName() {
        return hospitalDepartName;
    }

    public void setHospitalDepartName(String hospitalDepartName) {
        this.hospitalDepartName = hospitalDepartName;
    }

    public String getHospitalPlaceName() {
        return hospitalPlaceName;
    }

    public void setHospitalPlaceName(String hospitalPlaceName) {
        this.hospitalPlaceName = hospitalPlaceName;
    }

    public String getWasteBaseId() {
        return wasteBaseId;
    }

    public void setWasteBaseId(String wasteBaseId) {
        this.wasteBaseId = wasteBaseId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWasteClassifyId() {
        return wasteClassifyId;
    }

    public void setWasteClassifyId(String wasteClassifyId) {
        this.wasteClassifyId = wasteClassifyId;
    }
}
