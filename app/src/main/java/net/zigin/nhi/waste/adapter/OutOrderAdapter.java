package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.OutOrderBean;
import net.zigin.nhi.waste.bean.ReceiverBean;
import net.zigin.nhi.waste.view.print.OutOrderDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class OutOrderAdapter extends RecyclerView.Adapter<OutOrderAdapter.ViewHolder>{

    public List<OutOrderBean> list;

    public OutOrderAdapter(List<OutOrderBean> list) {
        this.list = list;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyData(List<OutOrderBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_out_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OutOrderBean bean = list.get(position);
        holder.mNum.setText(bean.getCode());
        holder.mTime.setText(bean.getCreateTime());
        ReceiverBean vo = bean.getSysUserRevicerVo();
        holder.mReceiver.setText(vo.getRealName());

        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), OutOrderDetailActivity.class);
            intent.putExtra("wasteOutId", bean.getId());
            intent.putExtra("receiver", vo.getRealName());
            intent.putExtra("mobile", vo.getMobile());
            intent.putExtra("carCode", vo.getCarCode());
            intent.putExtra("ckCode", bean.getCode());
            intent.putExtra("realWeight", bean.getWeight());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mNum;
        private TextView mTime;
        private TextView mReceiver;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mNum = itemView.findViewById(R.id.num);
            mTime = itemView.findViewById(R.id.time);
            mReceiver = itemView.findViewById(R.id.receiver);
        }
    }
}
