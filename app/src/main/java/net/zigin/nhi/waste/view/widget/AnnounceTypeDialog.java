package net.zigin.nhi.waste.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.AnnounceTypeAdapter;
import net.zigin.nhi.waste.adapter.IAnnounceTypeData;

import java.util.ArrayList;
import java.util.List;

public class AnnounceTypeDialog extends Dialog {

    private View mViewNone;
    private Button mClose;
    private RecyclerView mRvType;
    private Button mConfirm;
    private Activity activity;
    private IAnnounceTypeData dd;
    private String type;

    public static AnnounceTypeDialog mInstance;

    public AnnounceTypeDialog(@NonNull Context context, Activity activity, IAnnounceTypeData dd) {
        super(context);
        this.activity = activity;
        this.dd = dd;
    }

    //单例保证每次弹出的dialog唯一
    public static AnnounceTypeDialog getInstance(Context context, Activity activity, IAnnounceTypeData dd) {
        if (mInstance == null) {
            synchronized (AnnounceTypeDialog.class) {
                if (mInstance == null) {
                    mInstance = new AnnounceTypeDialog(context, activity, dd);
                }
            }
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_announce_type);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mViewNone = findViewById(R.id.view_none);
        mClose = findViewById(R.id.close);
        mRvType = findViewById(R.id.rv_type);
        mConfirm = findViewById(R.id.confirm);

        Window window = this.getWindow();
        window.setBackgroundDrawable(null);
        // 设置宽高
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置弹出的动画效果
        window.setWindowAnimations(R.style.AnimBottom);
        mViewNone = window.findViewById(R.id.view_none);
        mViewNone.getBackground().setAlpha(0);

        mRvType.setLayoutManager(new GridLayoutManager(activity, 3));
    }


    @SuppressLint("NotifyDataSetChanged")
    private void initData() {
        List<String> list = new ArrayList<>();
        list.add("科室");
        list.add("角色");
        list.add("接收人");
        AnnounceTypeAdapter typeAdapter = new AnnounceTypeAdapter(list);
        mRvType.setAdapter(typeAdapter);
        typeAdapter.setPosition(0);
        typeAdapter.setGetListener((position, text) -> {
            typeAdapter.setPosition(position);
            typeAdapter.notifyDataSetChanged();

            switch (text) {
                case "科室":
                    type = "depart";
                    break;
                case "角色":
                    type = "role";
                    break;
                case "接收人":
                    type = "staff";
                    break;
            }

        });

        mViewNone.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mClose.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mConfirm.setOnClickListener(v -> {
            dd.upDataUi(type);
            dismiss();
        });
    }
}
