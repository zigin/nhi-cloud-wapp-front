package net.zigin.nhi.waste.bean;

public class WasteClassifyBean {
    private String createTime;
    private String createUser;
    private String fillingType;//0重量1数量
    private String id;
    private String modifyTime;
    private String modifyUser;
    private String parentCode;//父类的wasteClassifyCode
    private String parentId;
    private String parentName;//父类的wasteClassifyName
    private String wasteClassifyCode;
    private String wasteClassifyName;

    public String getCreateTime() {
        return createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public String getFillingType() {
        return fillingType;
    }

    public String getId() {
        return id;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public String getParentCode() {
        return parentCode;
    }

    public String getParentId() {
        return parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public String getWasteClassifyCode() {
        return wasteClassifyCode;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public void setFillingType(String fillingType) {
        this.fillingType = fillingType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public void setWasteClassifyCode(String wasteClassifyCode) {
        this.wasteClassifyCode = wasteClassifyCode;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }
}
