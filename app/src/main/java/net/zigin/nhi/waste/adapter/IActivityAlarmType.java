package net.zigin.nhi.waste.adapter;

import net.zigin.nhi.waste.bean.AlarmBean;

import java.util.List;

/**
 * 预警类型筛选对话框通知activity的adapter更新list数据源
 */
public interface IActivityAlarmType {
    void upDataUi(List<AlarmBean> list);
}
