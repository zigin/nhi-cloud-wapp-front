package net.zigin.nhi.waste.utils;

import android.text.method.DigitsKeyListener;
import android.widget.EditText;

/*只能输入整数或者小数*/
public class WholeOrSmall {
    public static void wholeOrSmall(EditText editText,String type){
        if("1".equals(type)){
            String digits = "0123456789";
            editText.setHint("0");
            editText.setKeyListener(DigitsKeyListener.getInstance(digits));
        }else if("0".equals(type)){
            String digits = "0123456789.";
            editText.setHint("0.00");
            editText.setKeyListener(DigitsKeyListener.getInstance(digits));
        }
    }
}
