package net.zigin.nhi.waste;

import android.annotation.SuppressLint;
import android.app.Application;
import android.widget.Toast;

import com.clj.fastble.BleManager;
import com.clj.fastble.scan.BleScanRuleConfig;
import com.example.iscandemo.iScanInterface;
import com.gyf.immersionbar.ImmersionBar;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.olc.scan.ScanManager;
import com.olc.uhf.UhfAdapter;
import com.olc.uhf.UhfManager;
import com.zhifujiwen.jiaziisdk.CPrinterSdk;

import net.zigin.nhi.waste.view.collect.WasteCollectActivity;
import net.zigin.nhi.waste.view.main.LoginActivity;

import io.paperdb.Paper;
import me.goldze.mvvmhabit.base.BaseApplication;
import me.goldze.mvvmhabit.crash.CaocConfig;
import me.goldze.mvvmhabit.utils.KLog;

public class MainApplication extends BaseApplication {

    private static MainApplication sInstance;
    public static UhfManager mService;
    public static int m_version = 2;
    public static iScanInterface miScanInterface;
    public static CPrinterSdk oPrinterSdk = new CPrinterSdk();

    public static MainApplication getInstance() {
        return sInstance;
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        KLog.init(true);
        Paper.init(this);
        oPrinterSdk.Init();//初始化得力打印机
        oPrinterSdk.oCallback = new CPrinterSdk.Interface(){
            @Override
            public void ConnectSuccessfully(){
                SetTip1("得力打印机连接成功");
            }
            @Override
            public void ConnectFailed() {
                SetTip1("得力打印机连接失败");
            }

            @Override
            public void DeviceDisconnected() {
                SetTip1("得力打印机连接断开");
            }

            @Override
            public void Vtr_GetVersion(String sResult) { SetTip1(sResult); }
            @Override
            public void Vtr_GetModel(String sResult) { SetTip1(sResult); }
            @SuppressLint("DefaultLocale")
            @Override
            public void Vtr_GetDpi(int iResult) { SetTip1(String.format("%d", iResult)); }
            @Override
            public void Vtr_GetBluetoothMac(String sResult) { SetTip1(sResult); }
            @Override
            public void Vtr_GetBluetoothName(String sResult) { SetTip1(sResult); }
            @Override
            public void Vtr_GetSn(String sResult) { SetTip1(sResult); }
            @Override
            public void Vtr_GetStatus_String(String sResult) { SetTip1(sResult); }
            @SuppressLint("DefaultLocale")
            @Override
            public void Vtr_GetBatteryStatus(int iResult) { SetTip1(String.format("%d", iResult)); }
            @SuppressLint("DefaultLocale")
            @Override
            public void Vtr_GetPowerDownTime(int iResult) { SetTip1(String.format("%d", iResult)); }
            @SuppressLint("DefaultLocale")
            @Override
            public void Vtr_GetDensity(int iResult) { SetTip1(String.format("%d", iResult)); }
            @Override
            public void Vtr_GetPaperType_String(String sResult) { SetTip1(sResult); }
            @SuppressLint("DefaultLocale")
            @Override
            public void Vtr_GetCache(int iResult) { SetTip1(String.format("%d", iResult)); }
        };
        if(oPrinterSdk.IsBluetoothOpened()){
            //SetTip1("蓝牙已打开");
        }else{
            SetTip1("蓝牙未打开，请手动打开蓝牙");
        }

        //配置全局异常崩溃操作
        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT) //背景模式,开启沉浸式
                .enabled(true) //是否启动全局异常捕获
                .showErrorDetails(true) //是否显示错误详细信息
                .showRestartButton(true) //是否显示重启按钮
                .trackActivities(true) //是否跟踪Activity
                .minTimeBetweenCrashesMs(2000) //崩溃的间隔时间(毫秒)
                .errorDrawable(R.mipmap.ic_launcher) //错误图标
                .restartActivity(LoginActivity.class) //重新启动后的activity
                //.errorActivity(YourCustomErrorActivity.class) //崩溃后的错误activity
                //.eventListener(new YourCustomEventListener()) //崩溃后的错误监听
                .apply();

        //初始化扫码
        //创建iScanInterface实例化对象
        miScanInterface = new iScanInterface(this);
        miScanInterface.setOutputMode(0);
        miScanInterface.enablePlayBeep(true);//扫描成功后是否播放声音
        miScanInterface.enablePlayVibrate(true);//扫描成功后是否震动
        miScanInterface.continuousScan(false);//true：打开连扫模式（触发扫描按键开始连扫/停止连扫）false：关闭连扫模式
        miScanInterface.setIntervalTime(1000);

        //初始化Ble蓝牙框架
        BleManager.getInstance().init(sInstance);
        BleManager.getInstance()
                .enableLog(true)
                .setReConnectCount(1, 8000)
                .setOperateTimeout(8000);
        BleScanRuleConfig scanRuleConfig = new BleScanRuleConfig.Builder().setScanTimeOut(8000).build();
        BleManager.getInstance().initScanRule(scanRuleConfig);

        //初始化科大讯飞语音
        StringBuffer param = new StringBuffer();
        param.append("appid=" + getString(R.string.app_id));
        param.append(",");
        // 设置使用v5+
        param.append(SpeechConstant.ENGINE_MODE + "=" + SpeechConstant.MODE_MSC);
        SpeechUtility.createUtility(sInstance, param.toString());

        //初始化RFID
        mService = UhfAdapter.getUhfManager(sInstance);
        if (mService != null) {
            int status = mService.getStatus();
            m_version = ((status & 0xff0000) >> 16) & 0xff;
            if (mService.open()) {
                int b = 0;
            }
            boolean isOpen = mService.open();
        }

    }

    private void SetTip1(String sTip) {
        Toast.makeText(getApplicationContext(),sTip,Toast.LENGTH_SHORT).show();
    }
}
