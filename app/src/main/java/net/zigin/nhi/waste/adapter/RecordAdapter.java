package net.zigin.nhi.waste.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.RecordBean;

import java.util.List;

public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.ViewHolder>{

    public List<RecordBean> list;

    public RecordAdapter(List<RecordBean> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_record_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RecordBean bean = list.get(position);
        String content = bean.getContent();
        if (content.contains("null")) {
            content = content.replaceAll("null","").replaceAll("null,","");
        }
        holder.mTitle.setText(content);
        holder.mTime.setText(bean.getTime());

        if (position == list.size() - 1) {
            holder.mLine.setVisibility(View.INVISIBLE);
        }
        if (position == 0) {
            holder.mLineUp.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private TextView mTime;
        private View mLine;
        private View mLineUp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.title);
            mTime = itemView.findViewById(R.id.time);
            mLine = itemView.findViewById(R.id.line);
            mLineUp = itemView.findViewById(R.id.line_up);
        }
    }
}
