package net.zigin.nhi.waste.view.collect;

import android.content.Intent;
import android.os.Bundle;
import android.os.IScanListener;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gyf.immersionbar.ImmersionBar;
import com.yzq.zxinglibrary.android.CaptureActivity;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.CommonUtils;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.DialogManager;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

import static net.zigin.nhi.waste.MainApplication.miScanInterface;


/**
 * 医废收集科室扫码界面
 */
public class OfficeScanActivity extends AppCompatActivity{

    private LinearLayout mScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_office_scan);

        initView();
        miScanInterface.registerScan(miScanListener);
    }
    //数据回调监听
    private IScanListener miScanListener = new IScanListener() {

        /*
         * param data 扫描数据
         * param type 条码类型
         * param decodeTime 扫描时间
         * param keyDownTime 按键按下的时间
         * param imagePath 图片存储地址，通常用于ocr识别需求（需要先开启保存图片才有效）
         */
        @Override
        public void onScanResults(String data, int type, long decodeTime, long keyDownTime,String imagePath) {

            //解码失败
            if(data == null || data.isEmpty()){
                data = "  decode error ";
               // miScanInterface.scan_start();
            }else{
                miScanInterface.scan_stop();
                String finalData = data;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        jump(finalData);
                    }
                });
            }
        }
    };

    private void initView() {
        ImmersionBar.with(this).init();
        mScan = findViewById(R.id.scan);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mScan.setOnClickListener(v -> {
            if (miScanInterface != null) {
                //开启扫描
                miScanInterface.scan_start();
            } else {
                Intent intent = new Intent(OfficeScanActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 1003);
            }
        });
    }

    /**
     * 跳转页面
     *
     * @param qrCode 扫码结果
     */
    private void jump(String qrCode) {
        if(CommonUtils.isFastClick()) {
            return;
        }
        if (StringUtil.isNotEmpty(qrCode)) {
            Map<String, Object> map = new HashMap<>();
            map.put("content", qrCode);

            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .getOrCodeType(Paper.book().read(Constants.TOKEN).toString(), map);
            DialogManager.getInstance().showLoading(this);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Log.i("医废收集", data);
                    if ("hospitalDepart".equals(data)) {
                        Intent intent = new Intent(OfficeScanActivity.this, WasteCollectActivity.class);
                        intent.putExtra("scan_result", qrCode);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(OfficeScanActivity.this, "科室二维码扫描错误", Toast.LENGTH_SHORT).show();
                    }
                }

            });
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == 1003 && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra("codedContent");
                Log.i("扫码结果：", content);
                jump(content);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        miScanInterface.unregisterScan(miScanListener);
    }
}