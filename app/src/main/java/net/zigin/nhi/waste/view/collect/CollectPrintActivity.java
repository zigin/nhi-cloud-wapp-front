package net.zigin.nhi.waste.view.collect;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.BitmapUtils;
import net.zigin.nhi.waste.utils.CommonUtils;
import net.zigin.nhi.waste.view.print.WasteLabelActivity;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import Printer.PrintHelper;
import io.paperdb.Paper;
import io.reactivex.Observable;

import static net.zigin.nhi.waste.MainApplication.oPrinterSdk;

/**
 * 医废收集打印标签界面
 */
public class CollectPrintActivity extends AppCompatActivity {

    private TextView mHospitalName;
    private ImageView mTypeImg;
    private ImageView mCodeImg;
    private TextView mNumber;
    private TextView mOrigin;
    private TextView mTypeTv;
    private TextView mWeight;
    private TextView mHandover;
    private TextView tv_name;
    private TextView tv_unit;
    private TextView mTime;
    private Button mPrint;
    private WebView mWebView;

    private String hospitalName;
    private Set<String> wasteQRCodes = new HashSet<>();
    private PrintHelper printHelper;
    private String qrCode;
    private int connectStatus=-1;//打印机连接状态
    org.json.JSONObject printObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_print);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mHospitalName = findViewById(R.id.hospitalName);
        mTypeImg = findViewById(R.id.type_img);
        mCodeImg = findViewById(R.id.code_img);
        mNumber = findViewById(R.id.number);
        mOrigin = findViewById(R.id.origin);
        mTypeTv = findViewById(R.id.type_tv);
        mWeight = findViewById(R.id.weight);
        mHandover = findViewById(R.id.handover);
        mTime = findViewById(R.id.time);
        mPrint = findViewById(R.id.print);
        mWebView = findViewById(R.id.web_view);
        tv_name = findViewById(R.id.tv_name);
        tv_unit = findViewById(R.id.tv_unit);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onResume() {
        super.onResume();
        initData();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.enableSlowWholeDocumentDraw();
        }
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存
        mWebView.addJavascriptInterface(new PrintObject(), "connect");
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.loadUrl("file:///android_asset/collectPrint.html");
        connect();

    }
    //连接得力打印机
    private void connect(){
        new Thread(){
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                ArrayList<BluetoothDevice> aDevices = oPrinterSdk.GetDevices();
                if(aDevices!=null&&aDevices.size()>0){
                    if(oPrinterSdk.Connect(aDevices.get(0).getAddress()) == 0){
                        connectStatus=0;
                        Toast.makeText(CollectPrintActivity.this,"打印机连接成功",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(CollectPrintActivity.this,"打印机连接失败",Toast.LENGTH_SHORT).show();
                        connect();
                    }
                }else{
                    Toast.makeText(CollectPrintActivity.this,"附近没有搜到打印机！",Toast.LENGTH_SHORT).show();
                }
                Looper.loop();
            }
        }.start();
    }

    private void initData() {
        String id = Paper.book().read(Constants.COLLECTID);
        hospitalName = Paper.book().read(Constants.HOSPITAL_NAME);
        if (id != null) {
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .generateQrCode(Paper.book().read(Constants.TOKEN).toString(), id);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Log.i("下载二维码", data);
                    qrCode = data;
                    wasteQRCodes.add(data);
                    Paper.book().write(Constants.WASTE_QRCODES, wasteQRCodes);
                    Bitmap bitmap = BitmapUtils.createQRImage(data, 500, 500);
                    mCodeImg.setImageBitmap(bitmap);

                    Observable<BaseResponse<String>> observable1 = HttpRetrofitClient.getInstance().create(PublicService.class)
                            .getById(Paper.book().read(Constants.TOKEN).toString(), id);
                    HttpRetrofitClient.execute(observable1, new ApiCall<String>() {
                        @Override
                        protected void success(String data) {
                            Log.i("查询医废详情", data);
                            if (data != null) {
                                JSONObject object = JSON.parseObject(data);
                                mNumber.setText(object.getString("code"));
                                mOrigin.setText(object.getString("hospitalDepartName"));
                                mTypeTv.setText(object.getString("wasteClassifyName"));
                                mWeight.setText(object.getString("weight"));
                                if("1".equals(object.getString("fillingType"))){
                                    tv_unit.setText("个");tv_name.setText("数量：");
                                }
                                mHandover.setText(object.getString("handUserStaffName"));
                                mTime.setText(object.getString("createTime"));
                                if (hospitalName != null) {
                                    mHospitalName.setText(hospitalName);
                                }

                                printObject = new org.json.JSONObject();
                                try {
                                    printObject.put("hospitalName", mHospitalName.getText().toString());
                                    printObject.put("num", mNumber.getText().toString());
                                    printObject.put("departName", mOrigin.getText().toString());
                                    printObject.put("type", mTypeTv.getText().toString());
                                    printObject.put("weight", mWeight.getText().toString());
                                    printObject.put("handover", mHandover.getText().toString());
                                    printObject.put("time", mTime.getText().toString());
                                    printObject.put("qrCode", qrCode);
                                    printObject.put("fillingType", object.getString("fillingType"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Log.i("printObject", printObject.toString());
                                String call = "javascript:getCollectDetail(" + printObject.toString() + ")";
                                mWebView.loadUrl(call);
                            }
                        }
                    });
                }
            });
        }

        mPrint.setOnClickListener(v -> {
            if(connectStatus==0){
                if(CommonUtils.isFastClick()){
                    return;
                }
                mWebView.loadUrl("javascript:scanprint()");
                finish();
            }else{
                Toast.makeText(this,"打印机连接中，请稍后重试！",Toast.LENGTH_SHORT).show();
            }
        });
    }

    class PrintObject {
        @JavascriptInterface
        public void print(final int x, final int y, final int width, final int height) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        handler.sendEmptyMessage(1);
                    } catch (Exception | Error e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    @SuppressLint("HandlerLeak")
    Handler handler=new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            try {
                Thread.sleep(200);//
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int nw = mWebView.getWidth();
            int nh = mWebView.getHeight();
            Bitmap bitmap = Bitmap.createBitmap(nw, nh, Bitmap.Config.RGB_565);
            Canvas can = new Canvas(bitmap);
            mWebView.draw(can);
            int newWidth = nw;
            int newHeight = nh;
            if (nw > 400) {
                float rate = 380 * 1.0f / nw * 1.0f;
                newWidth = 380;
                newHeight = (int) (nh * rate);
            }
            //Thread.sleep(1000);//等待1s绘画完成
            bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
            Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0, newWidth, newHeight);
            if (oPrinterSdk.Tspl_SetSize(60, 40) != 0) return;//设置纸张大小
            if (oPrinterSdk.Tspl_SetGap(2, 0) != 0) return;//设置标签缝宽  连续纸可直接设置为 0 0
            if (oPrinterSdk.Tspl_SetDensity(7) != 0) return;//设置打印浓度0~15
            if (oPrinterSdk.Tspl_SetSpeed(5) != 0) return;//设置打印速度0~8
            if (oPrinterSdk.Tspl_Cls() != 0) return;//清空打印机打印缓存
            if (oPrinterSdk.Vtr_SetBitmap(newBitmap, 0, 0, true) != 0) return;//发送图片
            if (oPrinterSdk.Tspl_Print(1, 1) != 0) return;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        oPrinterSdk.Disconnect();
    }
}