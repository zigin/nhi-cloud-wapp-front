package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BitmapBean;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.utils.BitmapUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AnnounceImgAdapter extends RecyclerView.Adapter<AnnounceImgAdapter.ViewHolder> {

    public List<BitmapBean> list;
    private IAdapterUpData dd;
    private List<String> urlList = new ArrayList<>();

    public AnnounceImgAdapter(List<BitmapBean> list, IAdapterUpData dd) {
        this.list = list;
        this.dd = dd;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void notifyData(List<BitmapBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_announce_img, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BitmapBean bean = list.get(position);
        holder.mImg.setImageBitmap(bean.getBitmap());

        if (!bean.isUpload()) {
            String basePic = BitmapUtils.bitmapToBase64(bean.getBitmap());
            File file = null;
            try {
                file = BitmapUtils.decoderBase64File(basePic, holder.itemView.getContext());
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .upload(Paper.book().read("token").toString(), body);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String data) {
                        if (StringUtil.isNotEmpty(data)) {
                            Log.i("图片地址", data);
                            bean.setUpload(true);
                            urlList.add(data);
                            dd.upDataUi(urlList);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImg = itemView.findViewById(R.id.img);
        }
    }
}
