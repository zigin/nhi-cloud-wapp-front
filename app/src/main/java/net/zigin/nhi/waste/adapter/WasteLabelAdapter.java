 package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.WastePrintBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.BitmapUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class WasteLabelAdapter extends RecyclerView.Adapter<WasteLabelAdapter.ViewHolder>{

    public List<WastePrintBean> list;
    private IActivityUpData dd;
    private Set<WastePrintBean> newList;
    private boolean isItemCheck = false;

    public WasteLabelAdapter(List<WastePrintBean> list, IActivityUpData dd) {
        this.list = list;
        this.dd = dd;
        newList = new HashSet<>();
    }

    public void notifyData(List<WastePrintBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_waste_label, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WastePrintBean bean = list.get(position);
        holder.mWeight.setText(bean.getWeight());
        holder.mNumber.setText(bean.getCode());
        holder.mHospitalName.setText(bean.getHospitalBaseName());
        holder.mDepartName.setText(bean.getHospitalDepartName());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mPublisher.setText(bean.getHandUserStaffName());
        holder.mTime.setText(bean.getCreateTime());
        if("1".equals(bean.getFillingType())){
            holder.tv_name.setText("数量：");
            holder.tv_unit.setText("个");
        }else{
            holder.tv_name.setText("重量：");
            holder.tv_unit.setText("kg");
        }

        Bitmap bitmap = BitmapUtils.createQRImage(bean.getQrCode(), 500, 500);
        holder.mQrcode.setImageBitmap(bitmap);

        boolean isCheck = bean.isSelect();
        if (isCheck) {
            isItemCheck = true;
            holder.mImg.setImageResource(R.drawable.depot_in_selected);
        } else {
            isItemCheck = false;
            holder.mImg.setImageResource(R.drawable.depot_in_unselected);
        }

        newList.add(bean);
        Paper.book().write(Constants.NEW_WASTE_LABEL_LIST, newList);

        holder.mLayout.setOnClickListener(v -> {
            if (isItemCheck) {
                newList.remove(bean);
                isItemCheck = false;
                holder.mImg.setImageResource(R.drawable.depot_in_unselected);
                bean.setSelect(false);
                dd.upDataUi();

                newList.add(bean);
                Paper.book().write(Constants.NEW_WASTE_LABEL_LIST, newList);
            } else {
                newList.remove(bean);
                isItemCheck = true;
                holder.mImg.setImageResource(R.drawable.depot_in_selected);
                bean.setSelect(true);

                newList.add(bean);
                Paper.book().write(Constants.NEW_WASTE_LABEL_LIST, newList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLayout;
        private ImageView mImg;
        private TextView mWeight;
        private TextView mNumber;
        private TextView mHospitalName;
        private TextView mDepartName;
        private TextView mType;
        private TextView mPublisher;
        private TextView mTime;
        private TextView tv_name;
        private TextView tv_unit;
        private ImageView mQrcode;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImg = itemView.findViewById(R.id.img);
            mLayout = itemView.findViewById(R.id.layout);
            mWeight = itemView.findViewById(R.id.weight);
            mNumber = itemView.findViewById(R.id.number);
            mHospitalName = itemView.findViewById(R.id.hospitalName);
            mDepartName = itemView.findViewById(R.id.departName);
            mType = itemView.findViewById(R.id.type);
            mPublisher = itemView.findViewById(R.id.publisher);
            mTime = itemView.findViewById(R.id.time);
            tv_unit = itemView.findViewById(R.id.tv_unit);
            tv_name = itemView.findViewById(R.id.tv_name);
            mQrcode = itemView.findViewById(R.id.qrcode);
        }
    }
}
