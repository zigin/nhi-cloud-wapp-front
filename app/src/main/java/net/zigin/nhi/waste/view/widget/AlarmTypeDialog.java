package net.zigin.nhi.waste.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.AlarmAdapter;
import net.zigin.nhi.waste.adapter.FilterDepartAdapter;
import net.zigin.nhi.waste.adapter.IActivityAlarmType;
import net.zigin.nhi.waste.adapter.IDialogUpData;
import net.zigin.nhi.waste.adapter.NoDataAdapter;
import net.zigin.nhi.waste.bean.AlarmBean;
import net.zigin.nhi.waste.bean.DepartBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.alarm.AlarmActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class AlarmTypeDialog extends Dialog {

    private View mViewNone;
    private Button mClose;
    private RecyclerView mRvType;
    private Button mConfirm;
    private Activity activity;
    private RecyclerView mRvAlarm;
    private AlarmAdapter adapter;
    public static AlarmTypeDialog mInstance;
    private NoDataAdapter noDataAdapter = new NoDataAdapter();
    private IActivityAlarmType dd;

    private List<String> typeList = new ArrayList<>();
    private Map<String, Object> map = new HashMap<>();

    public AlarmTypeDialog(@NonNull Context context, Activity activity, RecyclerView mRvAlarm, AlarmAdapter adapter, IActivityAlarmType dd) {
        super(context);
        this.activity = activity;
        this.mRvAlarm = mRvAlarm;
        this.adapter = adapter;
        this.dd = dd;
    }

    //单例保证每次弹出的dialog唯一
    public static AlarmTypeDialog getInstance(Context context, Activity activity, RecyclerView mRvAlarm, AlarmAdapter adapter, IActivityAlarmType dd) {
        if (mInstance == null) {
            synchronized (AlarmTypeDialog.class) {
                if (mInstance == null) {
                    mInstance = new AlarmTypeDialog(context, activity, mRvAlarm, adapter, dd);
                }
            }
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_alarm_type);
        initView();
        initData();
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mViewNone = findViewById(R.id.view_none);
        mClose = findViewById(R.id.close);
        mRvType = findViewById(R.id.rv_type);
        mConfirm = findViewById(R.id.confirm);

        Window window = this.getWindow();
        window.setBackgroundDrawable(null);
        // 设置宽高
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置弹出的动画效果
        window.setWindowAnimations(R.style.AnimBottom);
        mViewNone = window.findViewById(R.id.view_none);
        mViewNone.getBackground().setAlpha(0);

        mRvType.setLayoutManager(new GridLayoutManager(activity, 3));
    }


    private void initData() {
        mViewNone.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mClose.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        getAlarmType();

        mConfirm.setOnClickListener(v -> {
            map.put("isPage", true);
            map.put("pageIndex", 1);
            map.put("pageSize", 20);
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .getAlarmList(Paper.book().read(Constants.TOKEN), map);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    if (StringUtil.isNotEmpty(data)) {
                        Log.i("告警列表", data);
                        List<AlarmBean> list = JSONArray.parseArray(data, AlarmBean.class);
                        if (list != null && list.size() > 0) {
                            adapter.notifyData(list);
                            dd.upDataUi(list);
                        } else {
                            mRvAlarm.setAdapter(noDataAdapter);
                        }
                    } else {
                        mRvAlarm.setAdapter(noDataAdapter);
                    }
                }
            });
            dismiss();
        });
    }

    /**
     * 筛选告警类型
     */
    @SuppressLint("NotifyDataSetChanged")
    private void getAlarmType() {
        typeList.add("全部预警类型");
        typeList.add("超重黄色预警");
        typeList.add("超重红色预警");
        typeList.add("超时黄色预警");
        typeList.add("超时红色预警");

        FilterDepartAdapter departAdapter = new FilterDepartAdapter(typeList);
        mRvType.setAdapter(departAdapter);
        departAdapter.setPosition(0);
        departAdapter.setGetListener((position, text) -> {
            departAdapter.setPosition(position);
            departAdapter.notifyDataSetChanged();

            switch (text) {
                case "全部预警类型":
                    map.remove("type");
                    Paper.book().delete(Constants.ALARM_TYPE);
                    break;
                case "超重黄色预警":
                    map.put("type", "weight_yellow");
                    Paper.book().write(Constants.ALARM_TYPE, "weight_yellow");
                    break;
                case "超重红色预警":
                    map.put("type", "weight_red");
                    Paper.book().write(Constants.ALARM_TYPE, "weight_red");
                    break;
                case "超时黄色预警":
                    map.put("type", "time_yellow");
                    Paper.book().write(Constants.ALARM_TYPE, "time_yellow");
                    break;
                case "超时红色预警":
                    map.put("type", "time_red");
                    Paper.book().write(Constants.ALARM_TYPE, "time_red");
                    break;
            }
        });

    }
}
