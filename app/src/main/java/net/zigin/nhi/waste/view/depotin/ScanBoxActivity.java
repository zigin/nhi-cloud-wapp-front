package net.zigin.nhi.waste.view.depotin;

import android.content.Intent;
import android.os.Bundle;
import android.os.IScanListener;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;
import com.yzq.zxinglibrary.android.CaptureActivity;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

import static net.zigin.nhi.waste.MainApplication.miScanInterface;

/**
 * 扫收集箱上二维码界面
 */
public class ScanBoxActivity extends AppCompatActivity{
    private LinearLayout mScan;

    private List<String> wasteBaseIDs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_box);
        initView();
        miScanInterface.registerScan(miScanListener);
    }
    //数据回调监听
    private IScanListener miScanListener = new IScanListener() {

        /*
         * param data 扫描数据
         * param type 条码类型
         * param decodeTime 扫描时间
         * param keyDownTime 按键按下的时间
         * param imagePath 图片存储地址，通常用于ocr识别需求（需要先开启保存图片才有效）
         */
        @Override
        public void onScanResults(String data, int type, long decodeTime, long keyDownTime,String imagePath) {

            //解码失败
            if(data == null || data.isEmpty()){
                data = "  decode error ";
                //miScanInterface.scan_start();
            }else{
                miScanInterface.scan_stop();
                String finalData = data;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getBoxData(finalData);
                    }
                });
            }
        }
    };

    private void initView() {
        ImmersionBar.with(this).init();
        mScan = findViewById(R.id.scan);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mScan.setOnClickListener(v -> {
            if (miScanInterface != null) {
                //开启扫描
                miScanInterface.scan_start();
            } else {
                Intent intent = new Intent(ScanBoxActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 1003);
            }
        });
    }

    private void getBoxData(String data) {
        if (StringUtil.isNotEmpty(data)) {
            getBoxDetail(data);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == 1003 && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra("codedContent");
                Log.i("扫码结果：", content);
                getBoxData(content);
            }
        }
    }

    /**
     * 获取集装箱详情
     */
    private void getBoxDetail(String qrCode) {
        Map<String, Object> map = new HashMap<>();
        map.put("content", qrCode);
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getListByBoxQrCode(Paper.book().read(Constants.TOKEN), map);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                wasteBaseIDs.clear();
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("获取医废箱详情", data);
                    JSONObject object = JSON.parseObject(data);
                    JSONObject wasteBox = object.getJSONObject("wasteBox");
                    String boxId = wasteBox.getString("id");
                    Paper.book().write(Constants.BOX_ID, boxId);
                    Paper.book().write(Constants.BOX_CODE, qrCode);

                    Intent intent = new Intent(ScanBoxActivity.this, BoxBindActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        miScanInterface.unregisterScan(miScanListener);
    }
}