package net.zigin.nhi.waste.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.fastjson.JSONArray;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.adapter.BatchTypeAdapter;
import net.zigin.nhi.waste.adapter.IAnnounceTypeData;
import net.zigin.nhi.waste.bean.WastePrintBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class BatchTypeDialog extends Dialog {

    private View mViewNone;
    private Button mClose;
    private RecyclerView mRvType;
    private Button mConfirm;
    private Activity activity;
    private IAnnounceTypeData dd;
    public static BatchTypeDialog mInstance;

    private List<String> typeList = new ArrayList<>();
    private List<WastePrintBean> wastePrintBeanList=new ArrayList<>();

    public BatchTypeDialog(@NonNull Context context, Activity activity, IAnnounceTypeData dd) {
        super(context);
        this.activity = activity;
        this.dd = dd;
    }

    //单例保证每次弹出的dialog唯一
    public static BatchTypeDialog getInstance(Context context, Activity activity, IAnnounceTypeData dd) {
        if (mInstance == null) {
            synchronized (BatchTypeDialog.class) {
                if (mInstance == null) {
                    mInstance = new BatchTypeDialog(context, activity, dd);
                }
            }
        }
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_waste_type);
        initView();
        initData();
        Paper.book().delete(Constants.BATCH_CHOOSE_TYPE);
    }

    private void initView() {
        ImmersionBar.with(activity, this).init();
        mViewNone = findViewById(R.id.view_none);
        mClose = findViewById(R.id.close);
        mRvType = findViewById(R.id.rv_type);
        mConfirm = findViewById(R.id.confirm);

        Window window = this.getWindow();
        window.setBackgroundDrawable(null);
        // 设置宽高
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        // 设置弹出的动画效果
        window.setWindowAnimations(R.style.AnimBottom);
        mViewNone = window.findViewById(R.id.view_none);
        mViewNone.getBackground().setAlpha(0);

        mRvType.setLayoutManager(new GridLayoutManager(activity, 3));
    }


    @SuppressLint("NotifyDataSetChanged")
    private void initData() {
        Observable<BaseResponse<String>> observable2 = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getWasteClassifyList(Paper.book().read(Constants.TOKEN));
        HttpRetrofitClient.execute(observable2, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    wastePrintBeanList = JSONArray.parseArray(data, WastePrintBean.class);
                }
                WastePrintBean wastePrintBean=new WastePrintBean();
                wastePrintBean.setWasteClassifyName("全部");
                wastePrintBean.setWasteClassifyCode("all");
                wastePrintBeanList.add(0,wastePrintBean);
                BatchTypeAdapter batchTypeAdapter = new BatchTypeAdapter(wastePrintBeanList);
                mRvType.setAdapter(batchTypeAdapter);
            }

        });

        mViewNone.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mClose.setOnClickListener(v -> {
            dismiss();
            ImmersionBar.destroy(activity, this);
        });

        mConfirm.setOnClickListener(v -> {
            String type=Paper.book().read(Constants.BATCH_CHOOSE_TYPE);
            if (StringUtil.isNotEmpty(type)) {
                dd.upDataUi(type);
            }
            dismiss();
        });
    }
}
