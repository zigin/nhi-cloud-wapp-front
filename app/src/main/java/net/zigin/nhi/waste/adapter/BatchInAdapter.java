package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BatchInBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.view.depotin.BatchItemInActivity;
import net.zigin.nhi.waste.view.depotout.BatchItemOutActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.paperdb.Paper;
import io.reactivex.Observable;

public class BatchInAdapter extends RecyclerView.Adapter<BatchInAdapter.ViewHolder>{

    public List<BatchInBean> list;
    private String collectorName;
    private IActivityUpData dd;
    private boolean isItemCheck = false;

    private List<BatchInBean> newList;

    public BatchInAdapter(List<BatchInBean> list, IActivityUpData dd, String collectorName) {
        this.list = list;
        this.dd = dd;
        this.collectorName = collectorName;
        newList = new ArrayList<>();
    }

    public void notifyData(List<BatchInBean> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_waste_batch, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BatchInBean bean = list.get(position);
        holder.mWasteNum.setText(bean.getCode());
        holder.mWeight.setText(bean.getWeight());
        holder.mHospitalName.setText(bean.getHospitalBaseName());
        holder.mDepartName.setText(bean.getHospitalDepartName());
        holder.mConsignee.setText(collectorName);
        holder.mPublisher.setText(bean.getHandUserStaffName());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mTime.setText(bean.getCreateTime());
        if("1".equals(bean.getFillingType())){
            holder.tv_name.setText("收集数量：");
            holder.tv_unit.setText("个");
        }else{
            holder.tv_name.setText("收集重量：");
            holder.tv_unit.setText("kg");
        }

        boolean isCheck = bean.isSelect();
        if (isCheck) {
            isItemCheck = true;
            holder.mImg.setImageResource(R.drawable.depot_in_selected);
        } else {
            isItemCheck = false;
            holder.mImg.setImageResource(R.drawable.depot_in_unselected);
        }

        newList.add(bean);
        Paper.book().write(Constants.NEW_BATCH_LIST, newList);

        holder.mLayout.setOnClickListener(v -> {
            if (isItemCheck) {
                newList.remove(bean);
                isItemCheck = false;
                holder.mImg.setImageResource(R.drawable.depot_in_unselected);
                bean.setSelect(false);
                dd.upDataUi();

                newList.add(bean);
                Paper.book().write(Constants.NEW_BATCH_LIST, newList);
            } else {
                newList.remove(bean);
                isItemCheck = true;
                holder.mImg.setImageResource(R.drawable.depot_in_selected);
                bean.setSelect(true);

                newList.add(bean);
                Paper.book().write(Constants.NEW_BATCH_LIST, newList);
            }
        });

        holder.mDepot.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), BatchItemInActivity.class);
            intent.putExtra("batchInItemID", bean.getId());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLayout;
        private ImageView mImg;
        private TextView mWasteNum;
        private TextView mWeight;
        private TextView mHospitalName;
        private TextView mDepartName;
        private TextView mType;
        private TextView mPublisher;
        private TextView mConsignee;
        private TextView mTime;
        private TextView tv_name;
        private TextView tv_unit;
        private Button mDepot;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout);
            mWasteNum = itemView.findViewById(R.id.waste_num);
            mImg = itemView.findViewById(R.id.img);
            mWeight = itemView.findViewById(R.id.weight);
            mHospitalName = itemView.findViewById(R.id.hospitalName);
            mDepartName = itemView.findViewById(R.id.departName);
            mType = itemView.findViewById(R.id.type);
            mPublisher = itemView.findViewById(R.id.publisher);
            mConsignee = itemView.findViewById(R.id.consignee);
            mTime = itemView.findViewById(R.id.time);
            mDepot = itemView.findViewById(R.id.depot);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_unit = itemView.findViewById(R.id.tv_unit);
        }
    }
}
