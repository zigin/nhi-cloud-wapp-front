package net.zigin.nhi.waste.view.dispatch;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import net.zigin.nhi.waste.R;


/**
 * 添加接收人界面
 */
public class AddReceiverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_receiver);
    }
}