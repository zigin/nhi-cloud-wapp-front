package net.zigin.nhi.waste.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BatchOutBean;

import java.util.ArrayList;
import java.util.List;

public class OfficeChildrenAdapter extends RecyclerView.Adapter<OfficeChildrenAdapter.ViewHolder>{

    public List<String> list;
    private int mPosition = -1;

    public OfficeChildrenAdapter(List<String> list) {
        this.list = list;
    }

    public void notifyData(List<String> beanList) {
        if (beanList != null) {
            list = new ArrayList<>();
            list.addAll(beanList);
        }
        notifyDataSetChanged();
    }

    public interface GetListener {
        void onClick(int position, String text);
    }

    private GetListener getListener;

    public void setGetListener(GetListener getListener) {
        this.getListener = getListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_children_office, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String text = list.get(position);
        holder.mChild.setText(text);

        holder.mLayout.setOnClickListener(v -> {
            getListener.onClick(position, text);
            notifyDataSetChanged();
        });

        if (position == getPosition()) {
            holder.mChild.setTextColor(Color.parseColor("#34BAFF"));
        }else{
            holder.mChild.setTextColor(Color.parseColor("#333333"));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout mLayout;
        private TextView mChild;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout);
            mChild = itemView.findViewById(R.id.child);
        }
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        this.mPosition = position;
    }
}
