package net.zigin.nhi.waste.view.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.depotout.OutPrintActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Printer.PrintHelper;
import io.paperdb.Paper;
import io.reactivex.Observable;

public class PrintDialog extends Dialog {

    private RelativeLayout mSub;
    private EditText mCount;
    private RelativeLayout mAdd;
    private Button mCancel;
    private Button mConfirm;
    private JSONArray dtos;
    private Activity activity;
    private String mode;
    private WebView webView;

    @SuppressLint("SetJavaScriptEnabled")
    public PrintDialog(@NonNull Context context, Activity activity, JSONArray dtos, String mode, WebView webView) {
        super(context);
        this.activity = activity;
        this.dtos = dtos;
        this.mode = mode;
        this.webView = webView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_print);
        initView();
        initListener();
    }

    private void initView() {
        mSub = findViewById(R.id.sub);
        mCount = findViewById(R.id.count);
        mAdd = findViewById(R.id.add);
        mCancel = findViewById(R.id.cancel);
        mConfirm = findViewById(R.id.confirm);
    }

    private void initListener() {
        mSub.setOnClickListener(v -> {
            int count = Integer.parseInt(mCount.getText().toString());
            if (count > 1) {
                count--;
                mCount.setText(String.valueOf(count));
            } else {
                Toast.makeText(getContext(), "至少打印一份！", Toast.LENGTH_SHORT).show();
            }
        });

        mAdd.setOnClickListener(v -> {
            int count = Integer.parseInt(mCount.getText().toString());
            count++;
            mCount.setText(String.valueOf(count));
        });

        mCancel.setOnClickListener(v -> {
            dismiss();
        });

        mConfirm.setOnClickListener(v -> {
            //根据打印次数循环调用打印方法
            int count = Integer.parseInt(mCount.getText().toString());
            while (count > 0) {
                webView.loadUrl("javascript:scanprint()");
                count--;
            }
            dismiss();
            /*Map<String, Object> map = new HashMap<>();
            switch (mode) {
                case "scan":    //扫码
                    map.put("wasteBaseDtos", dtos);
                    break;
                case "batch":   //批量
                    map.put("wasteBaseIds", Paper.book().read(Constants.BATCH_WASTE_IDS));
                    map.put("weight", Paper.book().read(Constants.BATCH_WEIGHT));
                    map.put("wasteClassifyCode", Paper.book().read(Constants.BATCH_TYPE));
                    String batchRemark = Paper.book().read(Constants.BATCH_REMARK);
                    if (StringUtil.isNotEmpty(batchRemark)) {
                        map.put("remark", batchRemark);
                    }
                    break;
                case "box":     //箱
                    JSONObject dto = new JSONObject();
                    dto.put("wasteClassifyCode", Paper.book().read(Constants.WASTE_OUT_BOX_TYPE));
                    dto.put("id", Paper.book().read(Constants.WASTE_OUT_BOX_ID));
                    dto.put("wasteBoxRecordId", Paper.book().read(Constants.WASTE_BOX_RECORD_ID));
                    dto.put("weight", Paper.book().read(Constants.WASTE_OUT_WEIGHT));
                    map.put("wasteBoxDto", dto);
                    String boxRemark = Paper.book().read(Constants.WASTE_OUT_REMARK);
                    if (StringUtil.isNotEmpty(boxRemark)) {
                        map.put("remark", boxRemark);
                    }
                    break;
            }
            map.put("revicerId", Paper.book().read(Constants.RECEIVER_ID));
            map.put("ckCode", Paper.book().read(Constants.CK_CODE));

            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .printMessage(Paper.book().read(Constants.TOKEN), map);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Toast.makeText(getContext(), "出库成功！", Toast.LENGTH_SHORT).show();
                    //根据打印次数循环调用打印方法
                    int count = Integer.parseInt(mCount.getText().toString());
                    while (count > 0) {
                        webView.loadUrl("javascript:scanprint()");
                        count --;
                    }
                    Paper.book().delete(Constants.WASTE_SCAN_DTOS);
                    Paper.book().delete(Constants.WASTE_BATCH_ITEM_DTOS);
                    dismiss();
                    activity.finish();
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    Paper.book().delete(Constants.WASTE_SCAN_DTOS);
                    Paper.book().delete(Constants.WASTE_BATCH_ITEM_DTOS);
                    dismiss();
                    activity.finish();
                }
            });*/
        });
    }
}
