package net.zigin.nhi.waste.bean;

public class WebWastePrintBean {
    private String code;
    private String createTime;
    private String handUserStaffName;
    private String hospitalBaseName;
    private String hospitalDepartName;
    private String id;
    private String wasteClassifyName;
    private String weight;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getHandUserStaffName() {
        return handUserStaffName;
    }

    public void setHandUserStaffName(String handUserStaffName) {
        this.handUserStaffName = handUserStaffName;
    }

    public String getHospitalBaseName() {
        return hospitalBaseName;
    }

    public void setHospitalBaseName(String hospitalBaseName) {
        this.hospitalBaseName = hospitalBaseName;
    }

    public String getHospitalDepartName() {
        return hospitalDepartName;
    }

    public void setHospitalDepartName(String hospitalDepartName) {
        this.hospitalDepartName = hospitalDepartName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "{" +
                "code='" + code + '\'' +
                ", createTime='" + createTime + '\'' +
                ", handUserStaffName='" + handUserStaffName + '\'' +
                ", hospitalBaseName='" + hospitalBaseName + '\'' +
                ", hospitalDepartName='" + hospitalDepartName + '\'' +
                ", id='" + id + '\'' +
                ", wasteClassifyName='" + wasteClassifyName + '\'' +
                ", weight='" + weight + '\'' +
                '}';
    }
}
