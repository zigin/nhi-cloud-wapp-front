package net.zigin.nhi.waste.view.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.IScanListener;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;
import com.yzq.zxinglibrary.android.CaptureActivity;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.MenuVoBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.SM4Utils;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.DialogManager;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import io.paperdb.Paper;
import io.reactivex.Observable;

import static net.zigin.nhi.waste.MainApplication.miScanInterface;


/**
 * 扫码登录界面
 */
public class LoginActivity extends AppCompatActivity {

    private EditText mAccount;
    private EditText mPassword;
    private Button mLogin;
    private LinearLayout mScanLogin;

    private List<MenuVoBean> menuList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (!isTaskRoot()) {
            if (getIntent() != null) {
                String action = getIntent().getAction();
                if (getIntent().hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
                    finish();
                    return;
                }
            }
        }

        getUserInfo();
        initView();
        initData();
        miScanInterface.registerScan(miScanListener);
    }
    //数据回调监听
    private IScanListener miScanListener = new IScanListener() {

        /*
         * param data 扫描数据
         * param type 条码类型
         * param decodeTime 扫描时间
         * param keyDownTime 按键按下的时间
         * param imagePath 图片存储地址，通常用于ocr识别需求（需要先开启保存图片才有效）
         */
        @Override
        public void onScanResults(String data, int type, long decodeTime, long keyDownTime,String imagePath) {

            //解码失败
            if(data == null || data.isEmpty()){
                data = "  decode error ";
               // miScanInterface.scan_start();
            }else{
                miScanInterface.scan_stop();
                String finalData = data;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        login(finalData);
                    }
                });
            }
        }
    };
    /**
     * 初始化控件
     */
    private void initView() {
        ImmersionBar.with(this).init();
        mAccount = findViewById(R.id.account);
        mPassword = findViewById(R.id.password);
        mLogin = findViewById(R.id.login);
        mScanLogin = findViewById(R.id.scan_login);
    }

    /**
     * 点击事件及接口
     */
    @SuppressLint("CheckResult")
    private void initData() {
//        mAccount.setText("1001");
//        mPassword.setText("123456");
//        mAccount.setText("18756271821");
//        mPassword.setText("271821");

        /**
         * 登录按钮点击事件
         */
        mLogin.setOnClickListener(v -> {
            if (StringUtil.isEmpty(mAccount.getText().toString())) {
                Toast.makeText(this, "请输入账号！", Toast.LENGTH_SHORT).show();
            } else if (StringUtil.isEmpty(mPassword.getText().toString())) {
                Toast.makeText(this, "请输入密码！", Toast.LENGTH_SHORT).show();
            } else {
                Map<String, Object> map = new HashMap<>();
                String name = mAccount.getText().toString();String password= mPassword.getText().toString();
                String charset = StandardCharsets.UTF_8.name();
                try {
                    byte[] myKeyBytes = Constants.myKey.getBytes(charset);
                    byte[] nameBytes = SM4Utils.encrypt_ECB_Padding(myKeyBytes, name.getBytes(charset));
                    byte[] passwordBytes = SM4Utils.encrypt_ECB_Padding(myKeyBytes, password.getBytes(charset));
                    name= ByteUtils.toHexString(nameBytes);
                    password= ByteUtils.toHexString(passwordBytes);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                map.put("name", name);
                map.put("password",password);
                Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                        .login(map);
                DialogManager.getInstance().showLoading(this);
                HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                    @Override
                    protected void success(String data) {
                        Log.i("登录", data);
                        DialogManager.getInstance().dismissLoading();
                        menuList.clear();
                        JSONObject object = JSON.parseObject(data);
                        menuList = JSONArray.parseArray(object.getString("menuVoList"), MenuVoBean.class);
                        if (menuList != null && menuList.size() > 0) {
                            Paper.book().write(Constants.MENULIST, menuList);
                        }
                        String userName = object.getString("name");
                        if (userName != null) {
                            Paper.book().write(Constants.USERNAME, userName);
                        }

                        String userStaffId = object.getString("userStaffId");
                        if (userStaffId != null) {
                            Paper.book().write(Constants.STAFFID, userStaffId);
                        }

                        String token = object.getString("token");
                        if (token != null) {
                            Paper.book().write(Constants.TOKEN, token);
                        }
                        Log.i("我的token", token);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });

        /**
         * 扫码登录按钮点击事件
         */
        mScanLogin.setOnClickListener(v -> {
            if (miScanInterface != null) {
                //开启扫描
                miScanInterface.scan_start();
            } else {
                Intent intent = new Intent(LoginActivity.this, CaptureActivity.class);
                startActivityForResult(intent, 1003);
            }
        });
    }

    /**
     * 登录跳转
     *
     * @param data 扫码结果
     */
    private void login(String data) {
        if (StringUtil.isNotEmpty(data)) {
            Map<String, Object> map = new HashMap<>();
            map.put("content", data);
            Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                    .loginWithQrCode(map);
            DialogManager.getInstance().showLoading(this);
            HttpRetrofitClient.execute(observable, new ApiCall<String>() {
                @Override
                protected void success(String data) {
                    Log.i("扫码登录", data);
                    menuList.clear();
                    JSONObject object = JSON.parseObject(data);
                    menuList = JSONArray.parseArray(object.getString("menuVoList"), MenuVoBean.class);
                    if (menuList != null && menuList.size() > 0) {
                        Paper.book().write(Constants.MENULIST, menuList);
                    }

                    String userName = object.getString("name");
                    if (userName != null) {
                        Paper.book().write(Constants.USERNAME, userName);
                    }

                    String userStaffId = object.getString("userStaffId");
                    if (userStaffId != null) {
                        Paper.book().write(Constants.STAFFID, userStaffId);
                    }

                    String token = object.getString("token");
                    if (token != null) {
                        Paper.book().write(Constants.TOKEN, token);
                    }

                    Log.i("我的token", token);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }


    /**
     * 设置个人信息
     */
    private void getUserInfo() {
        String id = Paper.book().read(Constants.STAFFID);
        String token = Paper.book().read(Constants.TOKEN);
        if (id == null || token == null) {
            return;
        }

        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getStaffInfoById(token, id);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @Override
            protected void success(String data) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 扫描二维码/条码回传
        if (requestCode == 1003 && resultCode == RESULT_OK) {
            if (data != null) {
                //返回的文本内容
                String content = data.getStringExtra("codedContent");
                Log.i("扫码结果：", content);
                login(content);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        miScanInterface.unregisterScan(miScanListener);
    }
}
