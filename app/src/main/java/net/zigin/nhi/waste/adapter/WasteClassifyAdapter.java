package net.zigin.nhi.waste.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.MenuVoBean;
import net.zigin.nhi.waste.bean.WasteClassifyBean;
import net.zigin.nhi.waste.utils.WeightNumberUtils;
import net.zigin.nhi.waste.utils.WholeOrSmall;

import java.util.List;
import java.util.Map;

public class WasteClassifyAdapter extends RecyclerView.Adapter<WasteClassifyAdapter.WasteClassifyHolder> {

    private List<WasteClassifyBean> wasteClassifyBeanList;
    private Map<String, Object> collectMap;
    private String wasteClassifyName;
    private String wasteClassifyCode;
    private TextView tv_name;
    private TextView tv_unit;
    private EditText mTvGravity;
    private Button button;
    int checkPosition=-1;

    public WasteClassifyAdapter(Map<String, Object> collectMap, List<WasteClassifyBean> wasteClassifyBeanList, TextView tv_name, TextView tv_unit, EditText mTvGravity, Button button) {
        this.wasteClassifyBeanList = wasteClassifyBeanList;
        this.collectMap = collectMap;
        this.wasteClassifyName = (String) collectMap.get("wasteClassifyName");
        this.wasteClassifyCode = (String) collectMap.get("wasteClassifyCode");
        this.tv_name = tv_name;
        this.tv_unit = tv_unit;
        this.button = button;
        this.mTvGravity = mTvGravity;
    }

    @Override
    public WasteClassifyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_waste_classify, parent, false);
        WasteClassifyHolder viewHolder = new WasteClassifyHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(WasteClassifyHolder holder, int position) {
        holder.rb_radio.setText(wasteClassifyBeanList.get(position).getWasteClassifyName());
        holder.rb_radio.setButtonDrawable(R.drawable.selector_radiobutton);
        holder.rb_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkPosition==-1){
                    checkPosition=position;
                    collectMap.put("wasteClassifyName", wasteClassifyBeanList.get(position).getWasteClassifyName());
                    collectMap.put("wasteClassifyCode",wasteClassifyBeanList.get(position).getWasteClassifyCode());
                    collectMap.put("fillingType",wasteClassifyBeanList.get(position).getFillingType());
                    WeightNumberUtils.changeUnit(tv_name,tv_unit,mTvGravity,wasteClassifyBeanList.get(position).getFillingType(),button);
                    WholeOrSmall.wholeOrSmall(mTvGravity,wasteClassifyBeanList.get(position).getFillingType());
                }else{
                    if(position==checkPosition){
                        checkPosition=-1;
                        collectMap.put("wasteClassifyName", wasteClassifyName);
                        collectMap.put("wasteClassifyCode",wasteClassifyCode);
                        collectMap.put("fillingType","0");
                        WeightNumberUtils.changeUnit(tv_name,tv_unit,mTvGravity,"0",button);
                        WholeOrSmall.wholeOrSmall(mTvGravity,"0");
                    }else{
                        checkPosition=position;
                        collectMap.put("wasteClassifyName", wasteClassifyBeanList.get(position).getWasteClassifyName());
                        collectMap.put("wasteClassifyCode",wasteClassifyBeanList.get(position).getWasteClassifyCode());
                        collectMap.put("fillingType",wasteClassifyBeanList.get(position).getFillingType());
                        WeightNumberUtils.changeUnit(tv_name,tv_unit,mTvGravity,wasteClassifyBeanList.get(position).getFillingType(),button);
                        WholeOrSmall.wholeOrSmall(mTvGravity,wasteClassifyBeanList.get(position).getFillingType());
                    }
                }
                notifyDataSetChanged();
            }
        });
        if(position==checkPosition){
            holder.rb_radio.setChecked(true);
        }else{
            holder.rb_radio.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return wasteClassifyBeanList.size();
    }

    class WasteClassifyHolder extends RecyclerView.ViewHolder {

        private RadioButton rb_radio;

        public WasteClassifyHolder(View itemView) {
            super(itemView);
            rb_radio = itemView.findViewById(R.id.rb_radio);
        }
    }

}