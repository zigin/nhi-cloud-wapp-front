package net.zigin.nhi.waste.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.BoxWasteBean;
import java.util.List;

public class BoxWasteAdapter extends RecyclerView.Adapter<BoxWasteAdapter.ViewHolder>{

    public List<BoxWasteBean> list;

    public BoxWasteAdapter(List<BoxWasteBean> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_box_waste, parent, false);
        return new ViewHolder(view);
    }

    @Override
    @SuppressLint("RecyclerView")
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BoxWasteBean bean = list.get(position);
        holder.mCollectWeight.setText(bean.getWeight());
        holder.mHospitalName.setText(bean.getHospitalBaseName());
        holder.mDepartName.setText(bean.getHospitalDepartName());
        holder.mType.setText(bean.getWasteClassifyName());
        holder.mHandover.setText(bean.getHandUserStaffName());
        holder.mCollector.setText(bean.getCollectUserStaffName());
        holder.mTime.setText(bean.getCreateTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mCollectWeight;
        private TextView mHospitalName;
        private TextView mDepartName;
        private TextView mType;
        private TextView mHandover;
        private TextView mCollector;
        private TextView mTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mCollectWeight = itemView.findViewById(R.id.collect_weight);
            mHospitalName = itemView.findViewById(R.id.hospitalName);
            mDepartName = itemView.findViewById(R.id.departName);
            mType = itemView.findViewById(R.id.type);
            mHandover = itemView.findViewById(R.id.handover);
            mCollector = itemView.findViewById(R.id.collector);
            mTime = itemView.findViewById(R.id.time);
        }
    }
}
