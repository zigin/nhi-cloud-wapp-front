package net.zigin.nhi.waste.view.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;

import net.zigin.nhi.waste.R;

/**
 * @Author:
 * @Description:
 */
public class YcProgressDialog extends Dialog {

    protected Context context;
    protected Activity activity;
    protected boolean keyCanBack = true;


    public YcProgressDialog(@NonNull Context context) {
        super(context, R.style.LoadingDialog);
        this.context = context;
        setWindowParams();
        setHelperOwnerActivity();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loading_msg);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        if (getWindow() != null) {
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.gravity = Gravity.CENTER;
            lp.alpha = 1.0f;
            lp.width = width;
            getWindow().setAttributes(lp);
        }
    }

    public void setDialogMsg(String msg) {
        TextView msgTv = findViewById(R.id.loading_dialog_msg);
        msgTv.setText(msg);
    }

    private void setWindowParams() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.color.transparent);
        }
    }


    /**
     * 设置 dialog 全屏
     * 注意：一定要在 setContentView 之后调用，否则无效
     */
    protected void setFullscreen() {
        Window window = getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void setHelperOwnerActivity() {
        if (context != null) {
            try {
                activity = (Activity) context;
            } catch (Exception e) {
                activity = null;
                e.printStackTrace();
            }
        }
        if (activity != null) {
            setOwnerActivity(activity);
        }
    }

    public void setCanceledOnTouchOutsideAndKeyBack(boolean canceled, boolean canBack) {
        setCanceledOnTouchOutside(canceled);
        setKeyCanBack(canBack);
    }

    public void setKeyCanBack(boolean bl) {
        this.keyCanBack = bl;
    }

    public boolean getKeyCanBack() {
        return keyCanBack;
    }

    @Override
    public void onBackPressed() {
        if (keyCanBack) {
            super.onBackPressed();
        }
    }

    protected void onTouchOutside() {
        Log.i("YcProgressDialog", "YcProgressDialog 抓去 dialog 以外的黑色区域");
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        /* 触摸弹窗外部 */
        if (isOutOfBounds(context, event)) {
            onTouchOutside();
        }
        return super.onTouchEvent(event);
    }

    private boolean isOutOfBounds(Context context, MotionEvent event) {
        final int x = (int) event.getX();
        final int y = (int) event.getY();
        final int slop = ViewConfiguration.get(context).getScaledWindowTouchSlop();
        final View decorView = getWindow().getDecorView();
        return (x < -slop) || (y < -slop)
                || (x > (decorView.getWidth() + slop))
                || (y > (decorView.getHeight() + slop));
    }

    @Override
    public void show() {
        try {
            if (activity != null && !activity.isFinishing() && !isShowing()) {
                super.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        try {
            if (activity != null && !activity.isFinishing() && isShowing()) {
                super.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
