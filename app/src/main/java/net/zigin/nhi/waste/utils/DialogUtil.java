package net.zigin.nhi.waste.utils;

import android.content.Context;
import android.view.View;

import net.zigin.nhi.waste.view.main.LoginActivity;

import me.drakeet.materialdialog.MaterialDialog;

public class DialogUtil {
    private Context context;
    private MaterialDialog mMaterialDialog;

    /**
     * 传入页面的dialog
     * @param context
     * @param layoutId
     */
    public DialogUtil(Context context , int layoutId) {
        this.context = context;
        mMaterialDialog = new MaterialDialog(context);
        mMaterialDialog.setContentView(layoutId);
    }

    /**
     * 显示标题和内容的dialog
     * @param context
     */
    public DialogUtil(Context context , String title , String message) {
        this.context = context;
        mMaterialDialog = new MaterialDialog(context)
                .setTitle(title)
                .setMessage(message);
    }

    public DialogUtil(Context context , String title , String message , String positive , View.OnClickListener positiveListener , String negative , View.OnClickListener netitiveListener) {
        this.context = context;
        mMaterialDialog = new MaterialDialog(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positive , positiveListener)
                .setNegativeButton(negative , netitiveListener);
    }


    /**
     * 获取dialog
     * @return
     */
    public MaterialDialog getmMaterialDialog() {
        return mMaterialDialog;
    }

    /**
     * 显示dialog
     */
    public void showDialog(){
        mMaterialDialog.show();
    }

    /**
     * dialog消失
     */
    public void dismissDialog(){
        mMaterialDialog.dismiss();
    }

}
