package net.zigin.nhi.waste.utils;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 收集重量和数量的转换
 */
public class WeightNumberUtils {
    //0重量1数量
    public static void changeUnit(TextView tv_name, TextView tv_unit, EditText editText, String type, Button button) {
        if("0".equals(type)){
            if(tv_name!=null)tv_name.setText("收集重量");
            if(tv_unit!=null)tv_unit.setText("kg");
            if(!editText.getText().toString().contains(".")){
                editText.setText("");
                editText.setHint("0.00");
            }
            button.setVisibility(View.VISIBLE);
        }else{
            if(tv_name!=null)tv_name.setText("收集数量");
            if(tv_unit!=null)tv_unit.setText("个");
            if(editText.getText().toString().contains(".")){
                editText.setText("");
                editText.setHint("0");
            }
            button.setVisibility(View.GONE);
        }
    }


    public static void changeUnit2(TextView tv_name,TextView tv_unit) {
        if(tv_name!=null)tv_name.setText("收集数量");
        if(tv_unit!=null)tv_unit.setText("个");
    }
    public static void changeUnit3(TextView tv_name,TextView tv_unit) {
        if(tv_name!=null)tv_name.setText("当前数量(可修改)");
        if(tv_unit!=null)tv_unit.setText("个");
    }
    public static void changeUnit4(TextView tv_name,TextView tv_unit) {
        if(tv_name!=null)tv_name.setText("数量差额：");
        if(tv_unit!=null)tv_unit.setText("个");
    }
}
