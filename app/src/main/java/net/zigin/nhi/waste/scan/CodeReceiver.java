package net.zigin.nhi.waste.scan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class CodeReceiver extends BroadcastReceiver {

    private CodeListener listener;

    public void setListener(CodeListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.startsWith("com.barcode.sendBroadcast")) {
            String data = intent.getStringExtra("BARCODE");
            if (listener != null) {
                listener.getData(data);
            }
        }
    }
}
