package net.zigin.nhi.waste.view.print;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gyf.immersionbar.ImmersionBar;

import net.zigin.nhi.waste.R;
import net.zigin.nhi.waste.bean.PrintMsgBean;
import net.zigin.nhi.waste.constants.Constants;
import net.zigin.nhi.waste.net.ApiCall;
import net.zigin.nhi.waste.net.BaseResponse;
import net.zigin.nhi.waste.net.HttpRetrofitClient;
import net.zigin.nhi.waste.net.PublicService;
import net.zigin.nhi.waste.utils.BitmapUtils;
import net.zigin.nhi.waste.utils.CommonUtils;
import net.zigin.nhi.waste.utils.StringUtil;
import net.zigin.nhi.waste.view.widget.OutOrderPrintDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import io.reactivex.Observable;

import static net.zigin.nhi.waste.MainApplication.oPrinterSdk;
import static net.zigin.nhi.waste.utils.BitmapUtils.getScreenshot;
import static net.zigin.nhi.waste.utils.BitmapUtils.newBitmap;

/**
 * 出库单详情打印界面
 */
public class OutOrderDetailActivity extends AppCompatActivity {

    private TextView mCarNumber;
    private TextView mName;
    private TextView mPhone;
    private WebView mWebView;
    private TextView mTotal;
    private TextView mOuter;
    private TextView tv_unit;
    private Button mBtnPrint;
    private int connectStatus=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        initView();
    }

    private void initView() {
        ImmersionBar.with(this).init();
        mCarNumber = findViewById(R.id.car_number);
        mName = findViewById(R.id.name);
        mPhone = findViewById(R.id.phone);
        tv_unit = findViewById(R.id.tv_unit);
        mWebView = findViewById(R.id.web_view);
        mTotal = findViewById(R.id.total);
        mOuter = findViewById(R.id.outer);
        mBtnPrint = findViewById(R.id.btn_print);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.enableSlowWholeDocumentDraw();
        }
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // 不加载缓存
        mWebView.addJavascriptInterface(new PrintObject(), "connect");
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.loadUrl("file:///android_asset/output.html");

        String realName = Paper.book().read(Constants.REALNAME);
        if (StringUtil.isNotEmpty(realName)) {
            mOuter.setText(realName);
        }

        mBtnPrint.setOnClickListener(v -> {
            if(connectStatus==0){
                OutOrderPrintDialog dialog = new OutOrderPrintDialog(this, OutOrderDetailActivity.this, mWebView);
                dialog.show();
            }else{
                Toast.makeText(this,"打印机连接中，请稍后重试！",Toast.LENGTH_SHORT).show();
            }
        });
        connect();
    }

    //连接得力打印机
    private void connect(){
        new Thread(){
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                ArrayList<BluetoothDevice> aDevices = oPrinterSdk.GetDevices();
                if(aDevices!=null&&aDevices.size()>0){
                    if(oPrinterSdk.Connect(aDevices.get(0).getAddress()) == 0){
                        connectStatus=0;
                        Toast.makeText(OutOrderDetailActivity.this,"打印机连接成功",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(OutOrderDetailActivity.this,"打印机连接失败",Toast.LENGTH_SHORT).show();
                        connect();
                    }
                }else{
                    Toast.makeText(OutOrderDetailActivity.this,"附近没有搜到打印机！",Toast.LENGTH_SHORT).show();
                }
                Looper.loop();
            }
        }.start();
    }

    private void getWasteOutInfo() {
        Intent intent = getIntent();
        String wasteOutId = intent.getStringExtra("wasteOutId");
        String receiver = intent.getStringExtra("receiver");
        String mobile = intent.getStringExtra("mobile");
        String carCode = intent.getStringExtra("carCode");
        String ckCode = intent.getStringExtra("ckCode");
        String realWeight = intent.getStringExtra("realWeight");
        mCarNumber.setText(carCode);
        mName.setText(receiver);
        mPhone.setText(mobile);
        Observable<BaseResponse<String>> observable = HttpRetrofitClient.getInstance().create(PublicService.class)
                .getWasteOutInfo(Paper.book().read(Constants.TOKEN), wasteOutId);
        HttpRetrofitClient.execute(observable, new ApiCall<String>() {
            @SuppressLint("DefaultLocale")
            @Override
            protected void success(String data) {
                if (StringUtil.isNotEmpty(data)) {
                    Log.i("出库单详情", data);
                    List<PrintMsgBean> printMsgList = JSONArray.parseArray(data, PrintMsgBean.class);
                    String fillingType = printMsgList.get(0).getFillingType();
                    if("1".equals(fillingType))tv_unit.setText(" 个");
                    mTotal.setText(realWeight);
                    //给webview传递数据
                    Map<String, Object> jsonData = new HashMap<>();
                    jsonData.put("ckCode", ckCode);
                    jsonData.put("dataList", printMsgList);
                    String json = JSONObject.toJSONString(jsonData);
                    Log.i("printObject", json);
                    String call = "javascript:getDetail('" + json + "')";
                    mWebView.loadUrl(call);
                }
            }
        });
    }

    class PrintObject {
        @JavascriptInterface
        public void print(final int x, final int y, final int width, final int height) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        handler.sendEmptyMessage(1);
                    } catch (Exception | Error e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    @SuppressLint("HandlerLeak")
    Handler handler=new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            try {
                Thread.sleep(200);//
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int nw = mWebView.getWidth();
            //获取网页在手机上真实的高度
            int nh = (int) (mWebView.getContentHeight()*mWebView.getScale());
            //获取Webview控件的高度
            int wHeight =mWebView.getHeight();
            //计算需要滚动次数
            int totalScrollCount = (int) (nh / wHeight);
            //剩余高度
            int surplusScrollHeight = nh - (totalScrollCount * wHeight);
            //1:打开缓存开关
            mWebView.setDrawingCacheEnabled(true);
            //存储图片容器
            List<Bitmap> cacheBitmaps = new ArrayList<>();
            for (int i = 0; i < totalScrollCount; i++) {
                mWebView.setScrollY(i * wHeight);
                cacheBitmaps.add(getScreenshot(mWebView));
            }
            //如果不能整除,需要额外滚动1次
            if (surplusScrollHeight > 0) {
                mWebView.setScrollY(totalScrollCount * wHeight);
                Bitmap bitmap = getScreenshot(mWebView);
                //截取最后剩下的图片 不需要全部
                Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, (int) (bitmap.getHeight()-surplusScrollHeight), bitmap.getWidth(), (int) surplusScrollHeight);
                cacheBitmaps.add(bitmap2);
            }
            //图片拼接-这就是最终的截屏图片
            Bitmap bitmap = newBitmap(cacheBitmaps);
            int newWidth = nw;
            int newHeight = nh;
            if (nw > 490) {
                float rate = 490 * 1.0f / nw * 1.0f;
                newWidth = 490;
                newHeight = (int) (nh * rate);
                Matrix matrix = new Matrix();
                matrix.postScale(rate, rate);
                bitmap= Bitmap.createBitmap(bitmap, 0, 0, nw,
                        nh, matrix, true);
            }

            int num=1;
            if(newHeight>280){
                if(newHeight%280<20){
                    num=newHeight/280;
                }else {
                    num = newHeight / 280 + 1;
                }
            }
            List<Bitmap> bitmaps=new ArrayList<>();
            for(int i=0;i<num;i++){
                bitmaps.add(Bitmap.createBitmap(bitmap, 0, i * 280, newWidth, (i==num-1)?(newHeight-280*i):280));
            }
            for (int i = 0; i < num; i++) {
                if (oPrinterSdk.Tspl_SetSize(70, 40)!= 0) return;//设置纸张大小
                if (oPrinterSdk.Tspl_SetGap(3, 0) != 0) return;//设置标签缝宽  连续纸可直接设置为 0 0
                if (oPrinterSdk.Tspl_SetDensity(7) != 0) return;//设置打印浓度0~15
                if (oPrinterSdk.Tspl_SetSpeed(5) != 0) return;//设置打印速度0~8
                if (oPrinterSdk.Tspl_Cls() != 0) return;//清空打印机打印缓存
                if (oPrinterSdk.Vtr_SetBitmap(bitmaps.get(i), 0, 0, true) != 0) return;//发送图片
                if (oPrinterSdk.Tspl_Print(1, 1) != 0) return;
            }

        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        oPrinterSdk.Disconnect();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);// 当打开新链接时，使用当前的 WebView，不会使用系统其他浏览器
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            //在这里执行你想调用的js函数
            getWasteOutInfo();
        }
    }
}