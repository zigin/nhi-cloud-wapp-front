package net.zigin.nhi.waste.utils;

import java.io.File;

public class FileUtil {
    public static void deleteFiles(File file){
        if(file.exists()){
            File[] files=file.listFiles();
            if(files!=null&&files.length!=0){
                for (File file1 : files) {
                    if(file1.exists())file1.delete();
                }
            }
        }
    }
}
