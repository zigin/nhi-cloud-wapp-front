package net.zigin.nhi.waste.bean;

public class RecordBean {
    private String afterUserStaffId;
    private String beforeUserStaffId;
    private String content;
    private String id;
    private String time;
    private String type;
    private String wasteBaseId;
    private String weight;

    public String getAfterUserStaffId() {
        return afterUserStaffId;
    }

    public void setAfterUserStaffId(String afterUserStaffId) {
        this.afterUserStaffId = afterUserStaffId;
    }

    public String getBeforeUserStaffId() {
        return beforeUserStaffId;
    }

    public void setBeforeUserStaffId(String beforeUserStaffId) {
        this.beforeUserStaffId = beforeUserStaffId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWasteBaseId() {
        return wasteBaseId;
    }

    public void setWasteBaseId(String wasteBaseId) {
        this.wasteBaseId = wasteBaseId;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
