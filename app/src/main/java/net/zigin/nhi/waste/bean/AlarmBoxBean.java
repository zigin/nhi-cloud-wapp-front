package net.zigin.nhi.waste.bean;

public class AlarmBoxBean {
    private String code;
    private String collectUserStaffId;
    private String collectUserStaffName;
    private String createTime;
    private String createUser;
    private String handUserStaffId;
    private String handUserStaffName;
    private String hospitalBaseId;
    private String hospitalBaseName;
    private String hospitalDepartId;
    private String hospitalDepartName;
    private String hospitalPlaceId;
    private String hospitalPlaceName;
    private String id;
    private String modifyTime;
    private String modifyUser;
    private String remark;
    private String status;
    private String userRevicerId;
    private String userRevicerName;
    private String wasteBoxCode;
    private String wasteBoxId;
    private String wasteBoxRecordId;
    private String wasteClassifyCode;
    private String wasteClassifyName;
    private String weight;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCollectUserStaffId() {
        return collectUserStaffId;
    }

    public void setCollectUserStaffId(String collectUserStaffId) {
        this.collectUserStaffId = collectUserStaffId;
    }

    public String getCollectUserStaffName() {
        return collectUserStaffName;
    }

    public void setCollectUserStaffName(String collectUserStaffName) {
        this.collectUserStaffName = collectUserStaffName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getHandUserStaffId() {
        return handUserStaffId;
    }

    public void setHandUserStaffId(String handUserStaffId) {
        this.handUserStaffId = handUserStaffId;
    }

    public String getHandUserStaffName() {
        return handUserStaffName;
    }

    public void setHandUserStaffName(String handUserStaffName) {
        this.handUserStaffName = handUserStaffName;
    }

    public String getHospitalBaseId() {
        return hospitalBaseId;
    }

    public void setHospitalBaseId(String hospitalBaseId) {
        this.hospitalBaseId = hospitalBaseId;
    }

    public String getHospitalBaseName() {
        return hospitalBaseName;
    }

    public void setHospitalBaseName(String hospitalBaseName) {
        this.hospitalBaseName = hospitalBaseName;
    }

    public String getHospitalDepartId() {
        return hospitalDepartId;
    }

    public void setHospitalDepartId(String hospitalDepartId) {
        this.hospitalDepartId = hospitalDepartId;
    }

    public String getHospitalDepartName() {
        return hospitalDepartName;
    }

    public void setHospitalDepartName(String hospitalDepartName) {
        this.hospitalDepartName = hospitalDepartName;
    }

    public String getHospitalPlaceId() {
        return hospitalPlaceId;
    }

    public void setHospitalPlaceId(String hospitalPlaceId) {
        this.hospitalPlaceId = hospitalPlaceId;
    }

    public String getHospitalPlaceName() {
        return hospitalPlaceName;
    }

    public void setHospitalPlaceName(String hospitalPlaceName) {
        this.hospitalPlaceName = hospitalPlaceName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserRevicerId() {
        return userRevicerId;
    }

    public void setUserRevicerId(String userRevicerId) {
        this.userRevicerId = userRevicerId;
    }

    public String getUserRevicerName() {
        return userRevicerName;
    }

    public void setUserRevicerName(String userRevicerName) {
        this.userRevicerName = userRevicerName;
    }

    public String getWasteBoxCode() {
        return wasteBoxCode;
    }

    public void setWasteBoxCode(String wasteBoxCode) {
        this.wasteBoxCode = wasteBoxCode;
    }

    public String getWasteBoxId() {
        return wasteBoxId;
    }

    public void setWasteBoxId(String wasteBoxId) {
        this.wasteBoxId = wasteBoxId;
    }

    public String getWasteBoxRecordId() {
        return wasteBoxRecordId;
    }

    public void setWasteBoxRecordId(String wasteBoxRecordId) {
        this.wasteBoxRecordId = wasteBoxRecordId;
    }

    public String getWasteClassifyCode() {
        return wasteClassifyCode;
    }

    public void setWasteClassifyCode(String wasteClassifyCode) {
        this.wasteClassifyCode = wasteClassifyCode;
    }

    public String getWasteClassifyName() {
        return wasteClassifyName;
    }

    public void setWasteClassifyName(String wasteClassifyName) {
        this.wasteClassifyName = wasteClassifyName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
